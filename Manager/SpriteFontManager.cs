﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class loads our spritefont
    /// </summary>
    static class SpriteFontManager
    {
        public static SpriteFont ActionJackson;

        public static void LoadContent()
        {
            ActionJackson = GameManager.LoadFont("ActionJackson36");
        }
    }
}
