﻿namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: SceneManager in order to Load Scenes with different methods
    /// </summary>
    static class SceneManager
    {
        public static Scene Scene { get; private set; }
        public static Scene SavedScene { get; set; }

        public static bool ChangedScene;

        public static void LoadScene<T>(bool delete = false) where T : Scene, new()
        {
            if (delete)
                GameManager.Delete();

            Scene = new T();
            Scene.LoadContent();
        }

        public static void LoadScene<T>(GameObject savedGameObject) where T : Scene, new()
        {
            GameManager.RemoveOtherGameObjects(savedGameObject);
            Scene = new T();
            Scene.LoadContent();
        }

        /// <summary>
        /// Alex: used it in order to switch scenes without losing the information 
        /// </summary>
        public static void LoadSavedScene()
        {
            Scene = SavedScene;
            ChangedScene = true;
        }
    }
}
