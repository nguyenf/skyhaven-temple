﻿using System.Collections.Generic;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: The manager was hard to deal with because of all our
    /// different needs, the both of us worked on it several times
    /// </summary>
    public static class CollisionManager
    {
        private static List<BoxCollider> addedColliders = new List<BoxCollider>();
        private static List<BoxCollider> colliders = new List<BoxCollider>();
        private static List<BoxCollider> removedColliders = new List<BoxCollider>();

        public static void AddCollider(BoxCollider collider)
        {
            addedColliders.Add(collider);
        }

        public static void RemoveCollider(BoxCollider collider)
        {
            colliders.Remove(collider);
        }

        /// <summary>
        /// A working and simple collision check
        /// </summary>
        /// <param name="boxCollider"></param>
        public static void CheckCollision(BoxCollider boxCollider)
        {
            for (int i = 0; i < colliders.Count; i++)
                if (boxCollider != colliders[i])
                    boxCollider.CheckCollision(colliders[i]);
        }

        /// <summary>
        /// Making it more flexible
        /// </summary>
        public static void ApplyChanges()
        {
            colliders.AddRange(addedColliders);
            addedColliders.Clear();
            removedColliders.ForEach(c => colliders.Remove(c));
            removedColliders.Clear();
        }
    }
}
