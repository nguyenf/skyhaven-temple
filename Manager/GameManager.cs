﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: The GameManager has been modified several times when we were facing repetition
    /// It is also the Manager which contains our GameObject list
    /// </summary>
    public static class GameManager
    {
        public static Random Random = new Random();
        public static GraphicsDevice GraphicsDevice;
        public static Enemy ActiveEnemy;

        private static List<GameObject> addedGameObjects = new List<GameObject>();
        private static List<GameObject> gameObjects = new List<GameObject>();
        private static List<GameObject> removedGameObjects = new List<GameObject>();

        public static Game Game { get; set; }
        public static ContentManager Content { get; set; }
        public static List<GameObject> GameObjects { get { return gameObjects; } }
        public static List<GameObject> AddedGameObjects { get { return addedGameObjects; } }

        /// <summary>
        /// With this algorithm we can freely change the gameObjects list while it is going through a 
        /// foreach by delaying the changes
        /// </summary>
        public static void ApplyChanges()
        {
            gameObjects.AddRange(addedGameObjects);
            addedGameObjects.Clear();
            removedGameObjects.ForEach(g => gameObjects.Remove(g));
            removedGameObjects.Clear();
        }

        public static void Delete()
        {
            GameObjects.ForEach(g => g.Destroy());
            GameObjects.Clear();
        }

        public static void RemoveOtherGameObjects(GameObject savedGameObject)
        {
            foreach (var gameObject in GameObjects)
            {
                if (gameObject == savedGameObject)
                    continue;

                gameObject.Destroy();
                Remove(gameObject);
            }
        }

        public static void Remove(GameObject gameObject)
        {
            removedGameObjects.Add(gameObject);
        }

        /// <summary>
        /// One of the methods we used a lot of times
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static GameObject GetGameObject<T>()
        {
            return gameObjects.Find(g => g is T);
        }

        public static void FillGameObjectList<T>(List<T> list) where T : GameObject
        {
            foreach (var gameObject in gameObjects)
                if (gameObject is T)
                    list.Add((T)gameObject);
        }

        public static void AddObject(GameObject gameObject)
        {
            addedGameObjects.Add(gameObject);
        }

        public static Texture2D LoadTexture(string name)
        {
            return Content.Load<Texture2D>(name);
        }

        public static SpriteFont LoadFont(string name)
        {
            return Content.Load<SpriteFont>(name);
        }

        public static Song LoadSong(string path)
        {
            return Content.Load<Song>(path);
        }

        public static SoundEffect LoadSoundEffect(string path)
        {
            return Content.Load<SoundEffect>(path);
        }

        public static void Exit()
        {
            Game.Exit();
        }
    }
}
