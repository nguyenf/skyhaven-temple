﻿using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Has been made by Simon and we did not change it: InputManager in order to provide an OnKeyPressed
    /// </summary>
    public static class InputManager
    {
        public delegate void InputEventHandler(Keys key);
        public static event InputEventHandler OnKeyPressed, OnKeyDown, OnKeyUp;

        private static Keys[] lastKeys = new Keys[0];
        private static Keys[] currentKeys = new Keys[0];

        public static void Update()
        {
            lastKeys = currentKeys;
            KeyboardState keyState = Keyboard.GetState();
            currentKeys = keyState.GetPressedKeys();

            foreach (var key in currentKeys)
            {
                if (OnKeyDown != null)
                    OnKeyDown(key);

                bool keyPressed = false;

                foreach (var lastKey in lastKeys)
                    if (key == lastKey)
                        keyPressed = true;

                if (!keyPressed && OnKeyPressed != null)
                    OnKeyPressed(key);
            }

            foreach (var key in lastKeys)
            {
                bool released = keyState.IsKeyUp(key);

                if (released && OnKeyUp != null)
                    OnKeyUp(key);
            }
        }
    }
}
