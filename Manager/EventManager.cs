﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: Used for the new architecture of components and gameObjects
    /// in order to make it more flexible
    /// </summary>
    public static class EventManager
    {
        public static Camera Camera;

        public delegate void DrawEvent(SpriteBatch spriteBatch);
        public static event DrawEvent OnDraw;

        public delegate void UpdateEvent(GameTime gameTime);
        public static event UpdateEvent OnUpdate;

        public static void Draw(SpriteBatch spriteBatch)
        {
            if (SceneManager.Scene is LevelScene)
            {
                if (Camera == null)
                    Camera = (Camera)GameManager.GameObjects.Find(g => g is Camera);

                spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, Camera?.GetMatrix()??Matrix.Identity);
            }

            else
                spriteBatch.Begin();

            if (OnDraw != null)
                OnDraw(spriteBatch);

            spriteBatch.End();
        }

        public static void Update(GameTime gameTime)
        {
            if (OnUpdate != null)
                OnUpdate(gameTime);
        }
    }
}
