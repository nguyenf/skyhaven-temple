﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class loads all the sound(effect)s we use in our game
    /// </summary>
    static class SoundManager
    {
        public static SoundEffect Collect;
        public static SoundEffect Explosion;
        public static SoundEffect HitShield;
        public static SoundEffect DogBark;
        public static SoundEffect NormalHit;
        public static SoundEffect Ouch;
        public static SoundEffect CastOverkill;
        public static SoundEffect Heal;
        public static SoundEffect Defend;

        private static Song MainTheme;
        private static Song BattleTheme;
        private static Song BossTheme;
        private static Song LoseTheme;
        private static Song WinTheme;

        public static void LoadContent()
        {
            Collect = GameManager.LoadSoundEffect("SFX/Collect.wav");
            Explosion = GameManager.LoadSoundEffect("SFX/Explosion.wav");
            HitShield = GameManager.LoadSoundEffect("SFX/HitShield.wav");
            DogBark = GameManager.LoadSoundEffect("SFX/DogBark.wav");
            NormalHit = GameManager.LoadSoundEffect("SFX/NormalHit.wav");
            Ouch = GameManager.LoadSoundEffect("SFX/Ouch.wav");
            CastOverkill = GameManager.LoadSoundEffect("SFX/Overkill.wav");
            Heal = GameManager.LoadSoundEffect("SFX/Heal.wav");
            Defend = GameManager.LoadSoundEffect("SFX/Defend.wav");

            MainTheme = GameManager.LoadSong("Music/MainTheme.wav");
            BattleTheme = GameManager.LoadSong("Music/BattleTheme.wav");
            BossTheme = GameManager.LoadSong("Music/BossTheme.wav");
            LoseTheme = GameManager.LoadSong("Music/LoseTheme.wav");
            WinTheme = GameManager.LoadSong("Music/WinTheme.wav");
        }

        /// <summary>
        /// chacks if the current scene has changed and plays suitable background music
        /// </summary>
        public static void PlayBackgroundMusic()
        {
            if (SceneManager.Scene is LevelScene || SceneManager.Scene is MainMenuScene)
                PlaySong(MainTheme, 1, true);
            else if (SceneManager.Scene is BossScene)
                PlaySong(BossTheme, 1);
            else if (SceneManager.Scene is BattleScene)
                PlaySong(BattleTheme, 1);
            else if (SceneManager.Scene is WinScene)
                PlaySong(WinTheme);
            else if (SceneManager.Scene is LoseScene)
                PlaySong(LoseTheme);
        }

        private static void PlaySong(Song song, float volume = 100, bool isRepeating = false)
        {
            if (SceneManager.ChangedScene)
            {
                MediaPlayer.Play(song);
                MediaPlayer.Volume = volume;
                MediaPlayer.IsRepeating = isRepeating;
                SceneManager.ChangedScene = false;
            }
        }
    }
}
