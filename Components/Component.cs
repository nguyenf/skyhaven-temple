﻿using System;
using System.Reflection;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: After switching the architecture
    /// the projekt itself has been created once again with components
    /// </summary>
    public abstract class Component
    {
        private GameObject gameObject;

        public GameObject GameObject
        {
            get { return gameObject; }
        }

        public void SetGameObject(GameObject gameObject)
        {
            if (this.gameObject != null)
                throw new Exception("GameObject is already set.");
            this.gameObject = gameObject;
        }

        public virtual void Destroy()
        {
            Type t = GetType();
            MethodInfo method = typeof(GameObject).GetMethod("RemoveComponent");
            MethodInfo genericMethod = method.MakeGenericMethod(t);
            genericMethod.Invoke(gameObject, null);
        }
    }
}
