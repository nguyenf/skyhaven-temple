﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: CoolDown updates itself and is mainly used for the player in order to 
    /// save performance
    /// </summary>
    class CoolDown : Component
    {
        public delegate void TimerEventHandler();
        public event TimerEventHandler OnCoolDown;

        private float timer;
        private bool isCoolingDown;

        public bool IsCoolingDown
        {
            get { return isCoolingDown; }
            set
            {
                if (isCoolingDown == value)
                    return;

                isCoolingDown = value;

                if (isCoolingDown)
                    timer = 0;
            }
        }

        public float Timer { get { return timer; } }

        public float Duration { get; set; }

        void Start()
        {
            EventManager.OnUpdate += Update;
        }

        /// <summary>
        /// Runs the call once the coolDown is over
        /// </summary>
        /// <param name="gameTime"></param>
        private void Update(GameTime gameTime)
        {
            if (IsCoolingDown)
            {
                timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (timer >= Duration)
                {
                    timer = 0;
                    IsCoolingDown = false;

                    if (OnCoolDown != null)
                        OnCoolDown();
                }
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
