﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    public class Movement : Component
    {
        public bool CanIMove = true;

        public void Move(int x, int y)
        {
            if (CanIMove && GameObject.HasComponent<Transform>())
                GameObject.GetComponent<Transform>().Translate(x, y);
        }
    }
}