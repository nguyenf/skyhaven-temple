﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Flocking algorithm
    /// </summary>
    class Flocking : Component
    {
        private const int Distance = 300;

        private List<Overkill> overkills = new List<Overkill>();

        private int neighborCount;
        private Vector2 alignment;
        private Vector2 cohesion;
        private Vector2 seperation;
        private Transform transform;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            GetNearbyAgents();
            PerformRules();
            Reset();
        }

        /// <summary>
        /// Combines the three rules 
        /// </summary>
        private void PerformRules()
        {
            transform.Velocity += ComputeAlignment() + ComputeCohesion() + ComputeSeperation();
        }

        private void Reset()
        {
            overkills.Clear();
            neighborCount = 0;
        }

        /// <summary>
        /// Counts the neighborCount and adds them to the list
        /// </summary>
        private void GetNearbyAgents()
        {
            foreach (var overkill in GameManager.GameObjects)
            {
                if (overkill == GameObject || !(overkill is Overkill) || !overkill.HasComponent<Transform>())
                    continue;

                Vector2 direction = overkill.GetComponent<Transform>().Position - transform.Position;
                float distance = direction.Length();

                if (distance <= Distance)
                {
                    overkills.Add((Overkill)overkill);
                    neighborCount++;
                }
            }
        }

        /// <summary>
        /// Computes the velocity of all agents. Alignment rules.
        /// </summary>
        /// <returns></returns>
        private Vector2 ComputeAlignment()
        {
            if (neighborCount == 0)
                return Vector2.Zero;

            for (int i = 0; i < overkills.Count; i++)
                alignment += overkills[i].GetComponent<Transform>().Velocity;

            alignment /= neighborCount;
            alignment.Normalize();
            return alignment;
        }

        /// <summary>
        /// Computes the position of all agents. Cohesion rules.
        /// </summary>
        /// <returns></returns>
        private Vector2 ComputeCohesion()
        {
            if (neighborCount == 0)
                return Vector2.Zero;

            for (int i = 0; i < overkills.Count; i++)
                cohesion += overkills[i].GetComponent<Transform>().Position;

            cohesion /= neighborCount;
            cohesion.Normalize();
            return cohesion;
        }

        /// <summary>
        /// Computes the directions of all agents towards each other. Seperation rules.
        /// </summary>
        /// <returns></returns>
        private Vector2 ComputeSeperation()
        {
            if (neighborCount == 0)
                return Vector2.Zero;

            for (int i = 0; i < overkills.Count; i++)
                seperation += overkills[i].GetComponent<Transform>().Position - GameObject.GetComponent<Transform>().Position;

            seperation /= neighborCount;
            seperation.Normalize();
            return seperation;
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
