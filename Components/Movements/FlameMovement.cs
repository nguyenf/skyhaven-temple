﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Flames movement
    /// </summary>
    class FlameMovement : Movement
    {
        private const int Movespeed = 300;

        private int counter;
        private Transform transform;
        private Animation animation;
        private string name = "Flame_";
        private List<Vector2> targetPositions = new List<Vector2>();

        public Vector2 NextPosition { get { return targetPositions[counter]; } }

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            animation = GameObject.AddComponent<Animation>();
            animation.LoadAtlas(true);
            animation.FrameDelay = 40;
            animation.PlayAnimation(name);
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            animation.PlayAnimation(name);

            if (SceneManager.Scene is LevelScene)
                Move(gameTime);
        }

        /// <summary>
        /// Sets up the positions, which are in the Flames.xml
        /// </summary>
        /// <param name="positionX"></param>
        /// <param name="positionY"></param>
        /// <param name="targetPositionX"></param>
        /// <param name="targetPositionY"></param>
        public void SetTargetPositions(float positionX, float positionY, float targetPositionX, float targetPositionY)
        {
            Vector2 position = new Vector2(positionX, positionY);
            Vector2 targetPosition = new Vector2(targetPositionX, targetPositionY);
            targetPositions.Add(position);
            targetPositions.Add(targetPosition);
        }

        /// <summary>
        /// This algorithm allows the gameObject to move to his targetPosition in a loop
        /// </summary>
        /// <param name="gameTime"></param>
        private void Move(GameTime gameTime)
        {
            Vector2 direction = NextPosition - transform.Position;
            float distance = direction.LengthSquared();
            direction.Normalize();

            if (distance != 0)
            {
                float movespeed = (float)gameTime.ElapsedGameTime.TotalSeconds * Movespeed;
                transform.Translate(Helper.Clamp(direction * movespeed, distance));
            }

            if (transform.Position == NextPosition)
            {
                counter++;

                if (counter == targetPositions.Count)
                    counter = 0;
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
