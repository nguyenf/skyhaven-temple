﻿using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Player movement
    /// </summary>
    class PlayerMovement : Movement
    {
        private const int Movespeed = 5;

        private Transform transform;
        private PlayerCombatBehaviour combat;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            combat = GameObject.GetComponent<PlayerCombatBehaviour>();

            InputManager.OnKeyDown += ProcessInput;
        }

        /// <summary>
        /// Key input and movement
        /// </summary>
        /// <param name="key"></param>
        private void ProcessInput(Keys key)
        {
            if (SceneManager.Scene is LevelScene && combat.Health > 0)
            {
                switch (key)
                {
                    case Keys.W:
                    case Keys.Up:
                        Move(0, -Movespeed);
                        break;
                    case Keys.S:
                    case Keys.Down:
                        Move(0, Movespeed);
                        break;
                    case Keys.D:
                    case Keys.Right:
                        Move(Movespeed, 0);
                        break;
                    case Keys.A:
                    case Keys.Left:
                        Move(-Movespeed, 0);
                        break;
                }
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
