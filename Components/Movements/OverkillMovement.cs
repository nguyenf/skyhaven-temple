﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Flocking, chasing etc. combined
    /// </summary>
    public class OverkillMovement : Movement
    {
        private const int Movespeed = 2;

        public string[] OverkillStates;

        private bool isFirstRunOver;
        private Transform transform;
        private Animation animation;
        private GameObject nearestEnemy;

        void Start()
        {
            OverkillStates = Helper.Deserialize<string[]>("Content/XML/OverkillStates");
            transform = GameObject.GetComponent<Transform>();
            animation = GameObject.AddComponent<Animation>();
            animation.LoadAtlas(true);
            animation.FrameDelay = 70;
            animation.PlayAnimation(OverkillStates[0]);
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            if (SceneManager.Scene is BattleScene)
                return;

            if (!isFirstRunOver)
                if (!animation.IsOver)
                    return;

            isFirstRunOver = true;
            Move(gameTime);
        }

        /// <summary>
        /// Chases the enemy
        /// </summary>
        /// <param name="gameTime"></param>
        private void ChaseEnemy(GameTime gameTime)
        {
            Vector2 direction = nearestEnemy.GetComponent<Transform>().Origin - transform.Position;
            direction.Normalize();
            ChangeAnimation(direction);
            transform.Translate(gameTime, direction * Movespeed);
        }

        /// <summary>
        /// Looks out for the enemy which is nearby and starts chasing after it.
        /// </summary>
        /// <param name="gameTime"></param>
        private void Move(GameTime gameTime)
        {
            nearestEnemy = null;
            float nearestDistance = float.PositiveInfinity;

            foreach (var enemy in GameManager.GameObjects)
            {
                if (!(enemy is Enemy) || !enemy.HasComponent<Transform>() || !GameObject.HasComponent<Transform>())
                    continue;

                Vector2 direction = enemy.GetComponent<Transform>().Origin - transform.Position;
                float distance = direction.LengthSquared();

                if (distance <= nearestDistance)
                {
                    nearestDistance = distance;
                    nearestEnemy = enemy;
                }
            }

            if (nearestEnemy != null)
                ChaseEnemy(gameTime);
        }

        /// <summary>
        /// Animations for both directions
        /// </summary>
        /// <param name="direction"></param>
        private void ChangeAnimation(Vector2 direction)
        {
            if (direction.X >= 0)
                animation.PlayAnimation(OverkillStates[1]);

            else
                animation.PlayAnimation(OverkillStates[2]);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
