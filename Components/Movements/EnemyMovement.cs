﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    class EnemyMovement : Movement
    {
        enum States { WalkRight, DeadRight, WalkLeft, DeadLeft}

        private const int ViewRange = 300;
        private const int Movespeed = 2;
        private const int Delay = 1;

        private float timer;
        private States currentState;
        private Animation animation;
        private Transform transform;
        private Vector2 moveDirection;
        private string[] states = { "Enemy_Right_W", "Enemy_Right_D", "Enemy_Left_W", "Enemy_Left_D" };

        private bool alredyBarked;

        void Start()
        {
            animation = GameObject.AddComponent<Animation>();
            animation.LoadAtlas(false);
            animation.FrameDelay = 50;
            animation.PlayAnimation(states[0]);
            transform = GameObject.GetComponent<Transform>();
            moveDirection = Direction(Movespeed);
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            if (SceneManager.Scene is LevelScene)
                ChangeDirection(gameTime, GameManager.GameObjects.Find(g => g.Name == "Player"), ViewRange, Movespeed);
        }

        private void Move()
        {
            transform.Position += moveDirection;
        }

        private void ChangeDirection(GameTime gameTime, GameObject targetGameObject, int viewRange, int movespeed)
        {
            Vector2 direction = targetGameObject.GetComponent<Transform>().Position - transform.Position;
            float distance = direction.Length();
            direction.Normalize();

            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (distance <= viewRange)
            {
                ChangeAnimation(direction);

                transform.Position += direction * gameTime.ElapsedGameTime.Milliseconds * 0.3f;
                if (!alredyBarked)
                {
                    SoundManager.DogBark.Play();
                    alredyBarked = true;
                }
            }

            else if (timer < Delay)
                Move();

            else
            {
                moveDirection = Direction(movespeed);
                timer = 0;
                alredyBarked = false;
            }
        }

        private void ChangeAnimation(Vector2 direction)
        {
            if (direction.X >= 0)
                animation.PlayAnimation(states[0]);

            else
                animation.PlayAnimation(states[2]);
        }

        private Vector2 Direction(int movespeed)
        {
            int choice = GameManager.Random.Next(4);

            switch (choice)
            {
                case 0:
                    return new Vector2(0, -movespeed);
                case 1:
                    return new Vector2(0, movespeed);
                case 2:
                    animation.PlayAnimation(states[0]);
                    return new Vector2(movespeed, 0);
                case 3:
                    animation.PlayAnimation(states[2]);
                    return new Vector2(-movespeed, 0);
                default:
                    return Vector2.Zero;
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
