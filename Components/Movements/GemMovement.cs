﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Movement of gem with field of view
    /// </summary>
    class GemMovement : Component
    {
        private const int ViewRange = 200;

        private Transform transform;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            if (GameManager.GameObjects.Exists(g => g is Player))
                SearchForPlayer(gameTime, (Player)GameManager.GameObjects.Find(g => g.Name == "Player"));
        }

        /// <summary>
        /// Field of view
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="target"></param>
        private void SearchForPlayer(GameTime gameTime, Player target)
        {
            Vector2 direction = target.GetComponent<Transform>().Origin - transform.Position;
            float distance = direction.Length();
            direction.Normalize();

            if (distance <= ViewRange)
                transform.Translate(direction * gameTime.ElapsedGameTime.Milliseconds * 0.2f);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
