﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// this is the text which appears in the battle scene over the head of the player/enemy/boss when he's his
    /// </summary>
    public enum HitKind { Poke, Scratch, Ripper, Block, Slash, Roar, Bite, Pathetic, Weak, Useless, Break, Die, Executer }

    /// <summary>
    /// Alex, Filip: this component is the base component for 
    /// the different combat behaviours of the player, enemies and boss 
    /// </summary>
    class Combat : Component
    {
        public HitKind CurrentHit;
        public bool BlockedHit;
        public bool HasAttacked;
        public bool HasRecovered;

        public int Health { get; set; }
        public int Power { get; set; }

        public void SetPoints(int power, int health)
        {
            Health = health;
            Power = power;
        }

        public virtual void Attack(GameObject other)
        {
            if (other == null)
                return;

            if (other.HasComponent<Combat>())
                other.GetComponent<Combat>().Health -= Power;
            else if (other.HasComponent<EnemyCombatBehaviour>())
                other.GetComponent<EnemyCombatBehaviour>().Health -= Power;
            else if (other.HasComponent<PlayerCombatBehaviour>())
                other.GetComponent<PlayerCombatBehaviour>().Health -= Power;
        }
    }
}
