﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: This BoxCollider has been created in order to get a better performance
    /// </summary>
    public class BoxCollider : Component
    {
        public delegate void CollisionEvent(GameObject gameObject);
        public event CollisionEvent OnCollisionStay, OnCollisionEnter, OnCollisionExit;

        private Renderer renderer;
        private Animation animation;
        private Transform transform;
        private Vector2 nextPosition;
        private Vector2 previousPosition;
        private List<BoxCollider> colliders = new List<BoxCollider>();

        public int Width { get; set; }
        public int Height { get; set; }
        public Vector2 Position { get { return nextPosition; } }

        /// <summary>
        /// The gameObject gets the values by the animation or renderer component
        /// </summary>
        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            nextPosition = GameObject.GetComponent<Transform>().Position;

            if (GameObject.HasComponent<Animation>())
            {
                animation = GameObject.GetComponent<Animation>();
                Width = animation.CurrentFrame.Bounds.Width;
                Height = animation.CurrentFrame.Bounds.Height;
            }

            else if (GameObject.HasComponent<Renderer>())
            {
                renderer = GameObject.GetComponent<Renderer>();
                Width = renderer.Sprite.Width;
                Height = renderer.Sprite.Height;
            }

            EventManager.OnUpdate += Update;

            CollisionManager.AddCollider(this);
        }

        /// <summary>
        /// The position is going to be saved in order to save performance
        /// </summary>
        /// <param name="gameTime"></param>
        private void Update(GameTime gameTime)
        {
            previousPosition = nextPosition;
            nextPosition = transform.Position;

            if (previousPosition != nextPosition)
                CollisionManager.CheckCollision(this);

            CollisionManager.ApplyChanges();
        }

        /// <summary>
        /// Standard BoxCollider
        /// </summary>
        /// <param name="other"></param>
        public void CheckCollision(BoxCollider other)
        {
            if (Position.X + Width < other.Position.X || other.Position.X + other.Width < Position.X || Position.Y + Height < other.Position.Y || other.Position.Y + other.Height < Position.Y)
            {
                if (OnCollisionExit != null && colliders.Contains(other))
                {
                    OnCollisionExit(other.GameObject);
                    colliders.Remove(other);
                }
                return;
            }

            if (OnCollisionStay != null && colliders.Contains(other))
            {
                OnCollisionStay(other.GameObject);
                return;
            }

            if (OnCollisionEnter != null && !colliders.Contains(other))
            {
                OnCollisionEnter(other.GameObject);
                colliders.Add(other);
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            CollisionManager.RemoveCollider(this);
            base.Destroy();
        }
    }
}
