﻿using System;
using System.Xml;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Animation component
    /// </summary>
    class Animation : Component
    {
        private float timer;
        private XmlReader xmlReader;
        private Transform transform;
        private int currentFrameIndex;
        private string currentAnimationName;
        private List<SpriteFrame> allFrames = new List<SpriteFrame>();
        private List<SpriteFrame> currentFrames = new List<SpriteFrame>();

        public bool IsOver
        {
            get
            {
                if (currentFrameIndex == currentFrames.Count - 1)
                    return true;

                else
                    return false;
            }
        }
        public string Name { get; private set; }
        public int FrameDelay { get; set; }
        public Texture2D SpriteAtlas { get; private set; }
        public SpriteFrame CurrentFrame { get; private set; }

        void Start()
        {
            Name = GameObject.Name;
            transform = GameObject.GetComponent<Transform>();
            EventManager.OnDraw += Draw;
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            UpdateAnimationFrame(gameTime);
        }

        private void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(SpriteAtlas, transform.Position, CurrentFrame.Bounds, Color.White);
        }

        public void LoadContent(Texture2D spriteAtlas, string atlasDataPath)
        {
            SpriteAtlas = spriteAtlas;
            LoadFrames(GameManager.Content.RootDirectory + atlasDataPath);
        }

        public void LoadAtlas(bool summons)
        {
            if (!summons)
            {
                SpriteAtlas = GameManager.LoadTexture("Atlas");
                LoadFrames(GameManager.Content.RootDirectory + "/XML/Atlas.xml");
            }

            else
            {
                SpriteAtlas = GameManager.LoadTexture("Summons");
                LoadFrames(GameManager.Content.RootDirectory + "/XML/Summons.xml");
            }
        }

        public void PlayAnimation(string name)
        {
            if (currentAnimationName != name)
            {
                currentFrames = allFrames.FindAll(animationFrame => animationFrame.Name.Contains(name));
                currentAnimationName = name;
                currentFrameIndex = 0;

                if (currentFrames.Count == 0)
                    throw new Exception(string.Format("No AnimationFrame found for {0}", name));
            }
        }

        private void UpdateAnimationFrame(GameTime gameTime)
        {
            timer += gameTime.ElapsedGameTime.Milliseconds;

            if (timer > FrameDelay)
            {
                timer = 0;
                if (currentFrameIndex < currentFrames.Count - 1)
                    currentFrameIndex++;

                else
                    currentFrameIndex = 0;

                CurrentFrame = currentFrames[currentFrameIndex];
            }
        }

        private void LoadFrames(string dataPath)
        {
            xmlReader = XmlReader.Create(dataPath);

            while (xmlReader.Read())
            {
                if (xmlReader.IsStartElement("sprite"))
                {
                    string frameName = xmlReader.GetAttribute("n");

                    if (frameName.Contains(Name))
                    {
                        SpriteFrame spriteFrame = new SpriteFrame();

                        spriteFrame.Name = frameName;
                        spriteFrame.Bounds.X = ConvertToInt("x");
                        spriteFrame.Bounds.Y = ConvertToInt("y");
                        spriteFrame.Bounds.Width = ConvertToInt("w");
                        spriteFrame.Bounds.Height = ConvertToInt("h");

                        allFrames.Add(spriteFrame);
                    }
                }
            }

            CurrentFrame = allFrames[currentFrameIndex];
        }

        private int ConvertToInt(string attribute)
        {
            return Convert.ToInt32(xmlReader.GetAttribute(attribute));
        }

        public override void Destroy()
        {
            EventManager.OnDraw -= Draw;
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
