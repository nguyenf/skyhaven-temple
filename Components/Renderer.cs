﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Standard Renderer
    /// </summary>
    class Renderer : Component
    {
        private Texture2D sprite;
        private Transform transform;
        private TileMapJapaneseVillage tileMap;

        public Color Color { get; set; }

        public Texture2D Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        /// <summary>
        /// AliceBlue! The name of the color really fits to the graphics of the game
        /// </summary>
        void Start()
        {
            Color = Color.AliceBlue;

            if (GameObject.HasComponent<TileMapJapaneseVillage>())
                tileMap = GameObject.GetComponent<TileMapJapaneseVillage>();
            else
            {
                LoadTexture();
                transform = GameObject.GetComponent<Transform>();
            }

            EventManager.OnDraw += Draw;
        }

        private void Draw(SpriteBatch spriteBatch)
        {
            if (!GameObject.HasComponent<Transform>() && !GameObject.HasComponent<TileMapJapaneseVillage>() || GameObject.HasComponent<Animation>())
                return;

            if (GameObject.HasComponent<TileMapJapaneseVillage>())
                tileMap.Draw(spriteBatch);

            else
                spriteBatch.Draw(Sprite, transform.Position, Color);
        }

        /// <summary>
        /// Each gameObject which has a renderer also needs a name and it will automatically load the sprite
        /// </summary>
        public void LoadTexture()
        {
            Sprite = GameManager.LoadTexture(GameObject.Name);
        }

        public override void Destroy()
        {
            EventManager.OnDraw -= Draw;
            base.Destroy();
        }
    }
}
