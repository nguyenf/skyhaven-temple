﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    class PlayerKeyInput : KeyInput
    {
        void Start()
        {
            InputManager.OnKeyPressed += ProcessInput;
        }

        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                case Keys.F:
                case Keys.Enter:
                    if (SceneManager.Scene is LevelScene)
                        ((Player)GameObject).CastOverkill();
                    break;
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
