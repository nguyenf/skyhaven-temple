﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Selector movement
    /// </summary>
    class SelectorKeyInput : Movement
    {
        private const int Movespeed = 10;

        private Transform transform;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
            InputManager.OnKeyDown += ProcessInput;
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            MoveMouse();
        }

        public new void Move(int x, int y)
        {
            transform.Translate(x, y);
            Mouse.SetPosition((int)transform.Position.X, (int)transform.Position.Y);
        }

        /// <summary>
        /// Key input. Movement.
        /// </summary>
        /// <param name="key"></param>
        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                case Keys.W:
                case Keys.Up:
                    Move(0, -Movespeed);
                    break;
                case Keys.S:
                case Keys.Down:
                    Move(0, Movespeed);
                    break;
                case Keys.D:
                case Keys.Right:
                    Move(Movespeed, 0);
                    break;
                case Keys.A:
                case Keys.Left:
                    Move(-Movespeed, 0);
                    break;
                case Keys.F12:
                    SceneManager.LoadScene<LevelScene>(true);
                    break;
            }
        }

        /// <summary>
        /// Position updates with the mouse
        /// </summary>
        private void MoveMouse()
        {
            MouseState mouseState = Mouse.GetState();

            transform.Position = new Vector2(mouseState.Position.X, mouseState.Position.Y);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            InputManager.OnKeyDown -= ProcessInput;
            base.Destroy();
        }
    }
}
