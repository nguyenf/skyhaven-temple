﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Transform component with a bunch of useful methods for the velocity and position
    /// </summary>
    class Transform : Component
    {
        private Vector2 position;

        private Vector2 velocity;

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public Vector2 Origin
        {
            get
            {
                return new Vector2(position.X + GameObject.GetComponent<Animation>().CurrentFrame.Bounds.Width / 2f,
                    position.Y + GameObject.GetComponent<Animation>().CurrentFrame.Bounds.Height / 2f);
            }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public void Translate(GameTime gameTime, Vector2 direction)
        {
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds * direction;
        }

        public void SetPosition(float x, float y)
        {
            position.X = x;
            position.Y = y;
        }

        public void Translate(int x, int y)
        {
            position.X += x;
            position.Y += y;
        }

        public void Translate(Vector2 translation)
        {
            position += translation;
        }
    }
}
