﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Player and what he is capable of
    /// </summary>
    public class PlayerBehaviour : KeyInput
    {
        enum Directions { Right, Left }

        public string[] PlayerStates;

        private float timer;
        private PlayerCombatBehaviour combat;
        private Animation animation;
        private Directions currentDirection;

        void Start()
        {
            PlayerStates = Helper.Deserialize<string[]>("Content/XML/PlayerStates");
            combat = GameObject.GetComponent<PlayerCombatBehaviour>();
            animation = GameObject.GetComponent<Animation>();
            animation.LoadAtlas(false);
            animation.PlayAnimation(PlayerStates[0]);
            animation.FrameDelay = 75;
            InputManager.OnKeyDown += ProcessInput;
            InputManager.OnKeyPressed += OnKeyPressed;
            EventManager.OnUpdate += Update;
        }

        private void OnKeyPressed(Keys key)
        {
            switch (key)
            {
                case Keys.Space:
                case Keys.Enter:
                    if (SceneManager.Scene is LevelScene)
                        ((Player)GameObject).CastOverkill();
                    break;
                case Keys.F11:
                    KillAllEnemies();
                    break;
            }
        }

        /// <summary>
        /// Obviously a cheat
        /// </summary>
        private void KillAllEnemies()
        {
            foreach (var enemy in GameManager.GameObjects)
            {
                if (!(enemy is Enemy))
                    continue;

                enemy.GetComponent<EnemyCombatBehaviour>().Health -= int.MaxValue;
            }
        }

        /// <summary>
        /// After 700 milliseconds the player is switching to the idle animation 
        /// just as we promised.
        /// </summary>
        /// <param name="gameTime"></param>
        private void Update(GameTime gameTime)
        {
            if (IsAlive())
            {
                timer += gameTime.ElapsedGameTime.Milliseconds;

                if (timer >= 700)
                    ChangeToIdleState();

                else
                    RunAnimation();
            }

            else
                RunDeathAnimation();
        }

        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                case Keys.D:
                case Keys.Right:
                    SetState(Directions.Right);
                    break;
                case Keys.A:
                case Keys.Left:
                    SetState(Directions.Left);
                    break;
                case Keys.F10:
                    combat.Health = 1;
                    break;

            }
        }

        /// <summary>
        /// Idle animation fitting to the direction the player is looking at
        /// </summary>
        private void ChangeToIdleState()
        {
            if (currentDirection == Directions.Right)
                animation.PlayAnimation(PlayerStates[0]);

            else if (currentDirection == Directions.Left)
                animation.PlayAnimation(PlayerStates[3]);
        }

        private void RunAnimation()
        {
            if (currentDirection == Directions.Right)
                Animate(PlayerStates[1]);

            else if (currentDirection == Directions.Left)
                Animate(PlayerStates[4]);
        }

        private void SetState(Directions state)
        {
            timer = 0;
            currentDirection = state;
        }

        private bool IsAlive()
        {
            if (combat.Health > 0)
                return true;

            else
                return false;
        }

        /// <summary>
        /// Plays Death animation and loads LoseScene 
        /// </summary>
        private void RunDeathAnimation()
        {
            if (currentDirection == Directions.Right)
                animation.PlayAnimation(PlayerStates[2]);

            else if (currentDirection == Directions.Left)
                animation.PlayAnimation(PlayerStates[5]);

            if (animation.IsOver)
            {
                SceneManager.LoadScene<LoseScene>(true);
                SceneManager.ChangedScene = true;
            }
        }

        private void Animate(string state)
        {
            animation.PlayAnimation(state);
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= OnKeyPressed;
            EventManager.OnUpdate -= Update;
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
