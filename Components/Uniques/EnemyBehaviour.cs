﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Enemy AI
    /// </summary>
    public class EnemyBehaviour : Movement
    {
        enum Directions { Right, Left }

        private const int ViewRange = 300;
        private const int Movespeed = 2;
        private const int Delay = 1;

        public bool IsDead;
        public int[] MaxValues;
        public string[] EnemyStates;

        private float timer;
        private Animation animation;
        private Transform transform;
        private Vector2 moveDirection;
        private Directions currentDirection;
        private EnemyCombatBehaviour enemyCombatBehaviour;

        private bool alreadyGrowled;

        void Start()
        {
            EnemyStates = Helper.Deserialize<string[]>("Content/XML/EnemyStates");
            MaxValues = Helper.Deserialize<int[]>("Content/XML/MaxValues");
            enemyCombatBehaviour = GameObject.GetComponent<EnemyCombatBehaviour>();
            animation = GameObject.AddComponent<Animation>();
            animation.LoadAtlas(false);
            animation.FrameDelay = 100;
            animation.PlayAnimation(EnemyStates[0]);
            transform = GameObject.GetComponent<Transform>();
            moveDirection = Direction(Movespeed);
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            if (SceneManager.Scene is LevelScene)
            {
                ChangeDirection(gameTime, GameManager.GameObjects.Find(g => g.Name == "Player"), ViewRange, Movespeed);
                PlayAnimation();
                CheckHealth(gameTime);
            }
        }

        /// <summary>
        /// Plays the animation of the direction
        /// </summary>
        private void PlayAnimation()
        {
            if (currentDirection == Directions.Right)
                animation.PlayAnimation(EnemyStates[0]);

            if (currentDirection == Directions.Left)
                animation.PlayAnimation(EnemyStates[2]);
        }

        private void Move()
        {
            transform.Translate(moveDirection);
        }

        /// <summary>
        /// This algorithm determins how the enemy is going to move and behave.
        /// When the player is in the field of view, he is going to chased. If this is not the case
        /// the enemy is going to change his direction when it is possible and will move or not when it is possible.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="targetGameObject"></param>
        /// <param name="viewRange"></param>
        /// <param name="movespeed"></param>
        private void ChangeDirection(GameTime gameTime, GameObject targetGameObject, int viewRange, int movespeed)
        {
            Vector2 direction = targetGameObject.GetComponent<Transform>().Position - transform.Position;
            float distance = direction.Length();
            direction.Normalize();

            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (distance <= viewRange)
            {
                ChangeAnimation(direction);

                transform.Translate(direction * gameTime.ElapsedGameTime.Milliseconds * 0.2f);
                if (!alreadyGrowled)
                {
                    SoundManager.DogBark.Play();
                    alreadyGrowled = true;
                }
            }

            else if (timer < Delay)
                Move();

            else
            {
                moveDirection = Direction(movespeed);
                timer = 0;
                alreadyGrowled = false;
            }
        }

        private void ChangeAnimation(Vector2 direction)
        {
            if (direction.X >= 0)
                currentDirection = Directions.Right;

            else
                currentDirection = Directions.Left;
        }

        /// <summary>
        /// Plays the death animation according to the direction
        /// </summary>
        /// <param name="gameTime"></param>
        private void CheckHealth(GameTime gameTime)
        {
            if (enemyCombatBehaviour.Health <= 0)
            {
                if (currentDirection == Directions.Right)
                    PlayDeathScenario(EnemyStates[1]);

                else
                    PlayDeathScenario(EnemyStates[3]);
            }
        }

        private void PlayDeathScenario(string state)
        {
            animation.PlayAnimation(state);
            IsDead = true;
            EventManager.OnUpdate -= Update;
        }

        /// <summary>
        /// The enemy gets a random direction or a Vector.Zero when the number is going to be 4 or
        /// when he is out of his value range which is in the Enemy.xml
        /// </summary>
        /// <param name="speed"></param>
        /// <returns></returns>
        private Vector2 Direction(int speed)
        {
            int choice = GameManager.Random.Next(5);

            switch (choice)
            {
                case 0:
                    if (transform.Position.Y <= MaxValues[0])
                        return Vector2.Zero;
                    return new Vector2(0, -speed);
                case 1:
                    if (transform.Position.Y >= MaxValues[1])
                        return Vector2.Zero;
                    return new Vector2(0, speed);
                case 2:
                    if (transform.Position.X >= MaxValues[2])
                        return Vector2.Zero;
                    currentDirection = Directions.Right;
                    return new Vector2(speed, 0);
                case 3:
                    if (transform.Position.X <= MaxValues[0])
                        return Vector2.Zero;
                    currentDirection = Directions.Left;
                    return new Vector2(-speed, 0);
                default:
                    return Vector2.Zero;
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
