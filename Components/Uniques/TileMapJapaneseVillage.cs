﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this component is used to draw the tiles from the japanese village sprite sheet
    /// to do so, this component needs a color map
    /// a color map is an image where each pixel has a specific color which stands for a particular tile on the sprite sheet
    /// </summary>
    class TileMapJapaneseVillage : Component
    {
        public const int TileWidth = 32;
        public const int TileHeight = 32;

        private int mapWidth;
        private int mapHeight;

        private Tile[,] mapField;

        private Texture2D tileset;
        private Texture2D mapImage;

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    Tile tile = mapField[x, y];
                    Vector2 position = new Vector2(x * TileWidth, y * TileHeight);
                    Rectangle sourceRect = new Rectangle((int)(tile.TileSheetX * TileWidth), (int)(tile.TileSheetY * TileHeight), TileWidth, TileHeight);

                    spriteBatch.Draw(tileset, position, sourceRect, Color.White);
                }
            }
        }

        /// <summary>
        /// with this method one can set the color map, the map width and map height (both are the number of pixels on the color map)
        /// </summary>
        public void SetMapField(string mapImage, int mapWidth, int mapHeight)
        {
            this.tileset = GameManager.LoadTexture("JapaneseVillage");
            this.mapImage = GameManager.LoadTexture(mapImage);
            LoadMapFromImage(this.mapImage, mapWidth, mapHeight);
        }

        /// <summary>
        /// this method initializes the map depending its with and height
        /// then it gets the color data from the color map and
        /// fills the map with tiles
        /// </summary>
        private void LoadMapFromImage(Texture2D image, int width, int height)
        {
            InitMapSize(width, height);
            Color[] data = LoadColorsFromImage(image);
            InitMapField(data);
        }

        /// <summary>
        /// initializes the map array
        /// </summary>
        private void InitMapSize(int width, int height)
        {
            mapWidth = width;
            mapHeight = height;
            mapField = new Tile[width, height];
        }

        /// <summary>
        /// returns the color data from the color map
        /// </summary>
        private Color[] LoadColorsFromImage(Texture2D image)
        {
            Color[] colors = new Color[image.Width * image.Height];
            image.GetData<Color>(colors);
            return colors;
        }

        /// <summary>
        /// fills the map array with the tiles depending on their position on the color map
        /// </summary>
        private void InitMapField(Color[] colors)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    int index = y * mapWidth + x;
                    Color colorType = colors[index];
                    mapField[x, y] = GetTileByColor(colorType, x, y);
                }
            }
        }

        /// <summary>
        /// returns the tiles depending on their color 
        /// </summary>
        private Tile GetTileByColor(Color color, int x, int y)
        {
            if (color == GetColor("lightTatamiHor0"))
                return new Tile(2, 0, x, y);
            else if (color == GetColor("lightTatamiHor1"))
                return new Tile(3, 0, x, y);
            else if (color == GetColor("lightTatamiVert0"))
                return new Tile(4, 0, x, y);
            else if (color == GetColor("lightTatamiVert1"))
                return new Tile(4, 1, x, y);

            else if (color == GetColor("darkTatamiHor0"))
                return new Tile(5, 0, x, y);
            else if (color == GetColor("darkTatamiHor1"))
                return new Tile(6, 0, x, y);
            else if (color == GetColor("darkTatamiVert0"))
                return new Tile(7, 0, x, y);
            else if (color == GetColor("darkTatamiVert1"))
                return new Tile(7, 1, x, y);

            else if (color == GetColor("woodFloor"))
                return new Tile(1, 0, x, y, true);

            else if (color == GetColor("window0"))
                return new Tile(6, 4, x, y);
            else if (color == GetColor("window1"))
                return new Tile(6, 5, x, y);

            else if (color == GetColor("downStairs0"))
                return new Tile(3, 15, x, y);
            else if (color == GetColor("downStairs1"))
                return new Tile(4, 15, x, y);
            else if (color == GetColor("downStairs2"))
                return new Tile(5, 15, x, y);
            else if (color == GetColor("downStairs3"))
                return new Tile(3, 16, x, y);
            else if (color == GetColor("downStairs4"))
                return new Tile(4, 16, x, y);
            else if (color == GetColor("downStairs5"))
                return new Tile(5, 16, x, y);

            else if (color == GetColor("upStairs0"))
                return new Tile(0, 14, x, y);
            else if (color == GetColor("upStairs1"))
                return new Tile(1, 14, x, y);
            else if (color == GetColor("upStairs2"))
                return new Tile(2, 14, x, y);
            else if (color == GetColor("upStairs3"))
                return new Tile(0, 15, x, y);
            else if (color == GetColor("upStairs4"))
                return new Tile(1, 15, x, y);
            else if (color == GetColor("upStairs5"))
                return new Tile(2, 15, x, y);
            else if (color == GetColor("upStairs6"))
                return new Tile(0, 16, x, y);
            else if (color == GetColor("upStairs7"))
                return new Tile(1, 16, x, y);
            else if (color == GetColor("upStairs8"))
                return new Tile(2, 16, x, y);

            else if (color == GetColor("foldingScreen0"))
                return new Tile(2, 2, x, y);
            else if (color == GetColor("foldingScreen1"))
                return new Tile(3, 2, x, y);
            else if (color == GetColor("foldingScreen2"))
                return new Tile(2, 3, x, y);
            else if (color == GetColor("foldingScreen3"))
                return new Tile(3, 3, x, y);
            else if (color == GetColor("foldingScreen4"))
                return new Tile(4, 3, x, y);


            else if (color == GetColor("whiteBed0"))
                return new Tile(6, 8, x, y);
            else if (color == GetColor("whiteBed1"))
                return new Tile(6, 9, x, y);
            else if (color == GetColor("whiteBed2"))
                return new Tile(6, 10, x, y);

            else if (color == GetColor("redBed0"))
                return new Tile(7, 8, x, y);
            else if (color == GetColor("redBed1"))
                return new Tile(7, 9, x, y);
            else if (color == GetColor("redBed2"))
                return new Tile(7, 10, x, y);

            else if (color == GetColor("table0"))
                return new Tile(0, 4, x, y);
            else if (color == GetColor("table1"))
                return new Tile(1, 4, x, y);
            else if (color == GetColor("table2"))
                return new Tile(2, 4, x, y);
            else if (color == GetColor("table3"))
                return new Tile(0, 5, x, y);
            else if (color == GetColor("table4"))
                return new Tile(1, 5, x, y);
            else if (color == GetColor("table5"))
                return new Tile(2, 5, x, y);
            else if (color == GetColor("table6"))
                return new Tile(0, 6, x, y);
            else if (color == GetColor("table7"))
                return new Tile(1, 6, x, y);
            else if (color == GetColor("table8"))
                return new Tile(2, 6, x, y);

            else if (color == GetColor("oven0"))
                return new Tile(3, 20, x, y);
            else if (color == GetColor("oven1"))
                return new Tile(3, 21, x, y);

            else if (color == GetColor("manyBottles"))
                return new Tile(7, 19, x, y);
            else if (color == GetColor("singleBottle"))
                return new Tile(6, 19, x, y);
            else if (color == GetColor("emptyCup"))
                return new Tile(6, 20, x, y);
            else if (color == GetColor("fullCup"))
                return new Tile(7, 20, x, y);
            else if (color == GetColor("foodBowl"))
                return new Tile(6, 21, x, y);
            else if (color == GetColor("pot"))
                return new Tile(0, 21, x, y);
            else if (color == GetColor("cuttingBoard"))
                return new Tile(0, 20, x, y);

            else if (color == GetColor("kitchen0"))
                return new Tile(0, 7, x, y);
            else if (color == GetColor("kitchen1"))
                return new Tile(1, 7, x, y);
            else if (color == GetColor("kitchen2"))
                return new Tile(2, 7, x, y);
            else if (color == GetColor("kitchen3"))
                return new Tile(0, 8, x, y);
            else if (color == GetColor("kitchen4"))
                return new Tile(1, 8, x, y);
            else if (color == GetColor("kitchen5"))
                return new Tile(2, 8, x, y);
            else if (color == GetColor("kitchen6"))
                return new Tile(0, 9, x, y);
            else if (color == GetColor("kitchen7"))
                return new Tile(1, 9, x, y);
            else if (color == GetColor("kitchen8"))
                return new Tile(2, 9, x, y);

            else if (color == GetColor("cupboard0"))
                return new Tile(0, 10, x, y);
            else if (color == GetColor("cupboard1"))
                return new Tile(1, 10, x, y);

            else if (color == GetColor("greens"))
                return new Tile(5, 13, x, y);

            else if (color == GetColor("blueTable0"))
                return new Tile(6, 17, x, y);
            else if (color == GetColor("blueTable1"))
                return new Tile(6, 18, x, y);
            else if (color == GetColor("crystalBall"))
                return new Tile(7, 17, x, y);

            else if (color == GetColor("sword0"))
                return new Tile(1, 19, x, y);
            else if (color == GetColor("sword1"))
                return new Tile(2, 19, x, y);

            else if (color == GetColor("cushionRed"))
                return new Tile(5, 7, x, y);
            else if (color == GetColor("cushionGray"))
                return new Tile(5, 8, x, y);

            else if (color == GetColor("bonsai0"))
                return new Tile(6, 12, x, y);
            else if (color == GetColor("bonsai1"))
                return new Tile(6, 13, x, y);

            else
                return new Tile(7, 23, x, y);

        }

        /// <summary>
        /// returns the color depending on the tile name 
        /// </summary>
        private Color GetColor(string name)
        {
            switch (name)
            {
                case "lightTatamiHor0":
                    return new Color(255, 0, 0);
                case "lightTatamiHor1":
                    return new Color(255, 255, 0);
                case "lightTatamiVert0":
                    return new Color(0, 0, 255);
                case "lightTatamiVert1":
                    return new Color(0, 255, 255);

                case "darkTatamiHor0":
                    return new Color(250, 0, 0);
                case "darkTatamiHor1":
                    return new Color(250, 250, 0);
                case "darkTatamiVert0":
                    return new Color(0, 0, 250);
                case "darkTatamiVert1":
                    return new Color(0, 250, 250);

                case "woodFloor":
                    return new Color(0, 0, 0);

                case "window0":
                    return new Color(100, 100, 100);
                case "window1":
                    return new Color(150, 150, 150);

                case "foldingScreen0":
                    return new Color(255, 0, 255);
                case "foldingScreen1":
                    return new Color(200, 0, 200);
                case "foldingScreen2":
                    return new Color(150, 0, 150);
                case "foldingScreen3":
                    return new Color(100, 0, 100);
                case "foldingScreen4":
                    return new Color(50, 0, 50);

                case "whiteBed0":
                    return new Color(30, 30, 30);
                case "whiteBed1":
                    return new Color(35, 35, 35);
                case "whiteBed2":
                    return new Color(37, 37, 37);
                case "redBed0":
                    return new Color(40, 40, 40);
                case "redBed1":
                    return new Color(45, 45, 45);
                case "redBed2":
                    return new Color(47, 47, 47);

                case "downStairs0":
                    return new Color(40, 150, 100);
                case "downStairs1":
                    return new Color(60, 150, 100);
                case "downStairs2":
                    return new Color(70, 150, 100);
                case "downStairs3":
                    return new Color(50, 150, 100);
                case "downStairs4":
                    return new Color(80, 150, 100);
                case "downStairs5":
                    return new Color(90, 150, 100);

                case "upStairs0":
                    return new Color(40, 100, 100);
                case "upStairs1":
                    return new Color(50, 100, 100);
                case "upStairs2":
                    return new Color(60, 100, 100);
                case "upStairs3":
                    return new Color(40, 101, 100);
                case "upStairs4":
                    return new Color(50, 101, 100);
                case "upStairs5":
                    return new Color(60, 101, 100);
                case "upStairs6":
                    return new Color(40, 102, 100);
                case "upStairs7":
                    return new Color(50, 102, 100);
                case "upStairs8":
                    return new Color(60, 102, 100);

                case "blueTable0":
                    return new Color(100, 150, 200);
                case "blueTable1":
                    return new Color(100, 200, 200);
                case "crystalBall":
                    return new Color(60, 120, 120);

                case "sword0":
                    return new Color(100, 50, 50);
                case "sword1":
                    return new Color(50, 50, 50);

                case "bonsai0":
                    return new Color(255, 255, 255);
                case "bonsai1":
                    return new Color(250, 250, 250);

                case "cushionRed":
                    return new Color(250, 100, 100);
                case "cushionGray":
                    return new Color(250, 150, 100);

                case "table0":
                    return new Color(200, 50, 0);
                case "table1":
                    return new Color(210, 60, 0);
                case "table2":
                    return new Color(220, 70, 0);
                case "table3":
                    return new Color(200, 50, 10);
                case "table4":
                    return new Color(210, 60, 10);
                case "table5":
                    return new Color(220, 70, 10);
                case "table6":
                    return new Color(200, 50, 20);
                case "table7":
                    return new Color(210, 60, 20);
                case "table8":
                    return new Color(220, 70, 20);

                case "oven0":
                    return new Color(0, 70, 70);
                case "oven1":
                    return new Color(0, 50, 50);

                case "manyBottles":
                    return new Color(0, 150, 0);
                case "singleBottle":
                    return new Color(0, 100, 0);
                case "emptyCup":
                    return new Color(0, 200, 0);
                case "fullCup":
                    return new Color(0, 50, 0);
                case "foodBowl":
                    return new Color(0, 215, 0);
                case "pot":
                    return new Color(0, 235, 0);
                case "cuttingBoard":
                    return new Color(0, 245, 0);

                case "kitchen0":
                    return new Color(25, 25, 100);
                case "kitchen1":
                    return new Color(25, 25, 125);
                case "kitchen2":
                    return new Color(25, 25, 150);
                case "kitchen3":
                    return new Color(25, 25, 101);
                case "kitchen4":
                    return new Color(25, 25, 126);
                case "kitchen5":
                    return new Color(25, 25, 151);
                case "kitchen6":
                    return new Color(70, 60, 50);
                case "kitchen7":
                    return new Color(80, 60, 50);
                case "kitchen8":
                    return new Color(90, 60, 50);

                case "cupboard0":
                    return new Color(21, 21, 21);
                case "cupboard1":
                    return new Color(22, 22, 22);

                case "greens":
                    return new Color(72, 72, 72);
            }

            throw new Exception("Wrong Color Name / Color not found");
        }
    }
}
