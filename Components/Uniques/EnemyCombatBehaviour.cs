﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class manages the combat behaviour of the enemies
    /// </summary>
    class EnemyCombatBehaviour : Combat
    {
        protected int HitCounter;

        private int criticalHit = 4;
        private int strongHit = 2;

        public int MaxCounter { get; set; }
        public bool IsCriticalLaunching
        {
            get
            {
                if (MaxCounter == HitCounter)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// if the player blocks the hit, the enemy deals no damage
        /// if the hit counter reaches the MaxCounter, the enemy makes his most powerfull hit
        /// else he either deals critical damage(30% chance) or regular damage
        /// </summary>
        public override void Attack(GameObject other)
        {
            if (other.GetComponent<PlayerCombatBehaviour>().BlockedHit)
            {
                CurrentHit = HitKind.Block;
                AttackPlayer(other);
                ResetHitCounter();

                return;
            }

            if (HitCounter == MaxCounter)
                DoSpecialAttack(other);
            else
                DoRegularAttack(other);
        }

        private void DoSpecialAttack(GameObject player)
        {
            CurrentHit = HitKind.Roar;
            Power *= criticalHit;
            AttackPlayer(player);
            Power /= criticalHit;
            ResetHitCounter();
        }

        private void DoRegularAttack(GameObject player)
        {
            int chance = GameManager.Random.Next(0, 3);

            if (chance == 0)
            {
                CurrentHit = HitKind.Bite;
                Power *= strongHit;
                AttackPlayer(player);
                Power /= strongHit;
            }
            else
            {
                CurrentHit = HitKind.Slash;
                AttackPlayer(player);
            }
        }

        protected void ResetHitCounter()
        {
            if (HitCounter > MaxCounter)
                HitCounter = 0;
        }

        private void AttackPlayer(GameObject player)
        {
            HitCounter++;
            HasAttacked = true;

            if (!player.GetComponent<PlayerCombatBehaviour>().BlockedHit)
            {
                player.GetComponent<PlayerCombatBehaviour>().Health -= Power;
                SoundManager.NormalHit.Play();
            }
            else
                SoundManager.HitShield.Play();
        }
    }
}
