﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this is the combat behaviour of the boss
    /// </summary>
    class BossCombatBehaviour : EnemyCombatBehaviour
    {
        private int MaxPower = 9999;

        /// <summary>
        /// if the attack counter reaches the MaxCounter, the boss deals massive damage (see MaxPower)
        /// else if the player blocks the special hit or any other hit, the boss deals half of his damage
        /// else he deals his full damage
        /// </summary>
        public override void Attack(GameObject other)
        {
            if (HitCounter == MaxCounter && !other.GetComponent<PlayerCombatBehaviour>().BlockedHit)
            {
                CurrentHit = HitKind.Executer;
                AttackPlayer(other, MaxPower);
            }
            else if (other.GetComponent<PlayerCombatBehaviour>().BlockedHit)
            {
                CurrentHit = HitKind.Break;
                Power /= 2;
                AttackPlayer(other, Power);
                Power *= 2;

                HitCounter++;
            }
            else
            {
                CurrentHit = HitKind.Die;
                AttackPlayer(other, Power);

                HitCounter++;
            }
        }

        private void AttackPlayer(GameObject player, int power)
        {
            player.GetComponent<PlayerCombatBehaviour>().Health -= power;
            SoundManager.Explosion.Play();
            SoundManager.NormalHit.Play();
            HasAttacked = true;
        }
    }
}