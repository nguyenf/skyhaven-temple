﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this is the combat behaviour of the player
    /// </summary>
    class PlayerCombatBehaviour : Combat
    {
        public int HealValue = 10;

        private int ripperDamage = 4;
        private int scratchDamage = 2;

        private int specialChance = 3;
        private int criticalChance = 2;

        /// <summary>
        /// the player has 
        /// 30% chance make his most powerfull hit
        /// 20% chance to deal critical damage
        /// </summary>
        public override void Attack(GameObject other)
        {
            int chance = GameManager.Random.Next(0, 10);

            if (chance < specialChance)
            {
                if (other is Boss)
                    CurrentHit = HitKind.Useless;
                else
                    CurrentHit = HitKind.Ripper;

                Power *= ripperDamage;
                AttackEnemy(other);
                Power /= ripperDamage;
                SoundManager.Explosion.Play();
            }
            else if (chance >= specialChance && chance < (specialChance + criticalChance))
            {
                if (other is Boss)
                    CurrentHit = HitKind.Weak;
                else
                    CurrentHit = HitKind.Scratch;

                Power *= scratchDamage;
                AttackEnemy(other);
                Power /= scratchDamage;
            }
            else
            {
                if (other is Boss)
                    CurrentHit = HitKind.Pathetic;
                else
                    CurrentHit = HitKind.Poke;

                AttackEnemy(other);
            }
        }

        public void Block()
        {
            BlockedHit = true;
            HasAttacked = true;
            SoundManager.Defend.Play();
        }

        public void Heal()
        {
            Health += HealValue;
            HasRecovered = true;
            HasAttacked = true;
            SoundManager.Heal.Play();
        }

        private void AttackEnemy(GameObject enemy)
        {
            if (enemy.HasComponent<EnemyCombatBehaviour>())
                enemy.GetComponent<EnemyCombatBehaviour>().Health -= Power;
            else if (enemy.HasComponent<BossCombatBehaviour>())
                enemy.GetComponent<BossCombatBehaviour>().Health -= Power;

            SoundManager.NormalHit.Play();
            HasAttacked = true;
        }
    }
}
