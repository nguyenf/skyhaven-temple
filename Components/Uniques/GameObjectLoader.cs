﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Used to summon objects freely. 
    /// In our case it has been used to summon the overkills
    /// </summary>
    class GameObjectLoader : Component
    {
        private Transform transform;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();
        }

        public void LoadOverkill(int amount, int area)
        {
            SoundManager.CastOverkill.Play();

            int[] values = { (int)transform.Position.X - area, (int)transform.Position.X + area,
                (int)transform.Position.Y - area, (int)transform.Position.Y + area };

            for (int i = 0; i < amount; i++)
            {
                int x = GameManager.Random.Next(values[0], values[1]);
                int y = GameManager.Random.Next(values[2], values[3]);

                GameManager.AddObject(new Overkill(new Vector2(x, y)));
            }
        }
    }
}
