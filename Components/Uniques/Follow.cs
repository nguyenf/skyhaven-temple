﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex, Filip: Filip started it and Alex debugged it 
    /// </summary>
    class Follow : Component
    {
        private const float ViewMargin = 0.35f;

        private Viewport viewport;
        private Player player;

        private Transform cameraTransform;
        private Transform playerTransform;

        private float cameraTranslationY;
        private float cameraTranslationX;

        private float marginWidth;
        private float marginHeight;
        private float marginLeft;
        private float marginRight;
        private float marginTop;
        private float marginBotton;


        private int playerSpriteWidth;
        private int playerSpriteHeight;


        void Start()
        {
            cameraTransform = GameObject.GetComponent<Transform>();
            viewport = GameManager.GraphicsDevice.Viewport;

            player = (Player)GameManager.GameObjects.Find(g => g is Player);
            playerTransform = player.GetComponent<Transform>();
            playerSpriteWidth = player.GetComponent<Animation>().CurrentFrame.Bounds.Width;
            playerSpriteHeight = player.GetComponent<Animation>().CurrentFrame.Bounds.Height;

            marginWidth = viewport.Width * ViewMargin;
            marginHeight = viewport.Height * ViewMargin;

            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            FixCamera();
        }

        /// <summary>
        /// The both of us were working on the camera.
        /// This algorithm provides a soft scrolling
        /// </summary>
        private void FixCamera()
        {
            marginLeft = cameraTransform.Position.X + marginWidth;
            marginRight = cameraTransform.Position.X + viewport.Width - marginWidth - playerSpriteWidth;
            marginTop = cameraTransform.Position.Y + marginHeight;
            marginBotton = cameraTransform.Position.Y + viewport.Height - marginHeight - playerSpriteHeight;

            cameraTranslationX = 0f;
            cameraTranslationY = 0f;

            if (playerTransform.Position.X < marginLeft)
                cameraTranslationX = playerTransform.Position.X - marginLeft;

            else if (playerTransform.Position.X > marginRight)
                cameraTranslationX = playerTransform.Position.X - marginRight;
          
            if (playerTransform.Position.Y < marginTop)
                cameraTranslationY = playerTransform.Position.Y - marginTop;
          
            else if (playerTransform.Position.Y > marginBotton)
                cameraTranslationY = playerTransform.Position.Y - marginBotton;
          
            cameraTransform.SetPosition(MathHelper.Clamp(cameraTransform.Position.X + cameraTranslationX, 0f, viewport.Width), 
                MathHelper.Clamp(cameraTransform.Position.Y + cameraTranslationY, 0f, viewport.Height));
        }
    }
}
