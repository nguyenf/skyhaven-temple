﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Healthpoints are visible 
    /// </summary>
    class Status : Component
    {
        private const float FontScale = 0.8f;
        private const int CoolDown = 5;

        private CoolDown coolDown;
        private Transform transform;
        private Vector2 offSet = new Vector2(20, 50);
        private EnemyCombatBehaviour enemyCombatBehaviour;
        private Vector2 coolDownOffSet = new Vector2(-5, -55);
        private PlayerCombatBehaviour playerCombatBehaviour;

        void Start()
        {
            transform = GameObject.GetComponent<Transform>();

            if (GameObject is Enemy)
                enemyCombatBehaviour = GameObject.GetComponent<EnemyCombatBehaviour>();

            else if (GameObject is Player)
            {
                playerCombatBehaviour = GameObject.GetComponent<PlayerCombatBehaviour>();
                coolDown = GameObject.GetComponent<CoolDown>();
            }

            EventManager.OnDraw += Draw;
        }

        /// <summary>
        /// CoolDown for overkills, and HP displayed
        /// </summary>
        /// <param name="spriteBatch"></param>
        private void Draw(SpriteBatch spriteBatch)
        {
            if (GameObject.HasComponent<EnemyCombatBehaviour>())
                spriteBatch.DrawString(SpriteFontManager.ActionJackson, enemyCombatBehaviour.Health.ToString(), transform.Position - offSet, Color.DarkRed, 0f, Vector2.Zero, FontScale, SpriteEffects.None, 0f);

            else
            {
                spriteBatch.DrawString(SpriteFontManager.ActionJackson, playerCombatBehaviour.Health.ToString(), transform.Position - offSet, Color.Green, 0f, Vector2.Zero, FontScale, SpriteEffects.None, 0f);
                if (coolDown.IsCoolingDown)
                    spriteBatch.DrawString(SpriteFontManager.ActionJackson, Math.Round((CoolDown - coolDown.Timer)).ToString(), transform.Position - coolDownOffSet, Color.DarkSlateBlue, 0f, Vector2.Zero, FontScale, SpriteEffects.None, 0f);
            }
        }

        public override void Destroy()
        {
            EventManager.OnDraw -= Draw;
            base.Destroy();
        }
    }
}
