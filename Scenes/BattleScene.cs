﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this is our battle scene
    /// </summary>
    class BattleScene : Scene
    {

        protected BattleMap BattleMap;
        protected UISelector Arrow;
        protected UIImage AttackEffect;

        protected Player Player;
        protected PlayerCombatBehaviour PlayerCombatBehaviour;
        protected UIImage PlayerImage;
        protected UIText PlayerHitKind;
        protected UIText PlayerHP;

        protected const float BattleEndDelay = 2000;
        protected float BattleEndCounter;
        protected float Timer;
        protected float NextHitDelay = 1;

        protected bool CreatedAttackEffect;

        private Enemy enemy;
        private EnemyCombatBehaviour enemyCombatBehaviour;
        private UIImage enemyImage;
        private UIText enemyHitKind;
        private UIText enemyHP;

        public override void LoadContent()
        {
            SceneManager.ChangedScene = true;

            CreateObjects();
            SetStartPositions();

            InputManager.OnKeyPressed += OnKeyPressed;
            EventManager.OnUpdate += OnUpdate;
        }

        /// <summary>
        /// Creates the objects which are needed in this scene
        /// </summary>
        private void CreateObjects()
        {
            BattleMap = new BattleMap();
            Arrow = Helper.Deserialize<UISelector>("Content/XML/Arrow");

            Player = (Player)GameManager.GetGameObject<Player>();
            PlayerCombatBehaviour = Player.GetComponent<PlayerCombatBehaviour>();
            PlayerImage = new UIImage("PlayerB", false);
            PlayerHitKind = Helper.Deserialize<UIText>("Content/XML/PlayerHitKind");
            PlayerHP = Helper.Deserialize<UIText>("Content/XML/PlayerHP");

            enemy = GameManager.ActiveEnemy;
            enemyCombatBehaviour = enemy.GetComponent<EnemyCombatBehaviour>();
            enemyImage = new UIImage("EnemyB", false);
            enemyHitKind = Helper.Deserialize<UIText>("Content/XML/EnemyHitKind");
            enemyHP = Helper.Deserialize<UIText>("Content/XML/EnemyHP");
        }

        /// <summary>
        /// Sets the created objects to their positions
        /// </summary>
        private void SetStartPositions()
        {
            Arrow.GetComponent<Transform>().Position = Arrow.Positions[0];

            PlayerImage.GetComponent<Transform>().Position = new Vector2(100, 380);
            PlayerHitKind.SetPosition();
            PlayerHP.SetPosition();

            enemyImage.GetComponent<Transform>().Position = new Vector2(550, 370);
            enemyHitKind.SetPosition();
            enemyHP.SetPosition();
        }

        private void OnUpdate(GameTime gameTime)
        {
            // if the next hit of an enemy is going to be critical
            // an explanation mark is displayed to warn the player
            if (enemyCombatBehaviour.IsCriticalLaunching)
                new ExclamationMark();

            PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
            enemyHP.Text = enemyCombatBehaviour.Health.ToString();

            // if the enemy is dead, the level scene is loaded after a short amount of time
            if (enemyCombatBehaviour.Health <= 0)
            {
                InputManager.OnKeyPressed -= OnKeyPressed;
                BattleEndCounter += gameTime.ElapsedGameTime.Milliseconds;

                if (BattleEndCounter >= BattleEndDelay)
                {
                    PlayerCombatBehaviour.HasAttacked = false;
                    SceneManager.LoadSavedScene();
                    EventManager.OnUpdate -= OnUpdate;
                    DestroyObjects();

                    EventManager.OnUpdate -= OnUpdate;
                }
            }

            // if the player dies, the PlayerBehaviour loads the lose scene
            else if (Player.GetComponent<PlayerCombatBehaviour>() == null)
            {
                InputManager.OnKeyPressed -= OnKeyPressed;
                EventManager.OnUpdate -= OnUpdate;
            }

            PlayerAttackAlgorithm(gameTime);
            EnemyAttackAlgorithm(gameTime);
        }

        /// <summary>
        /// if the player has attacked
        /// a slash effect is created on the enemy
        /// the current hit kind of the player is displayed over the enemies head
        /// 
        /// if the player heals or defends himself, no effect is shown
        /// 
        /// after a short delay the hit kind text is set back to its start position and isn't shown anymore
        /// and the enemy attacks the player
        /// </summary>
        private void PlayerAttackAlgorithm(GameTime gameTime)
        {
            if (PlayerCombatBehaviour.HasAttacked)
            {
                if (!PlayerCombatBehaviour.HasRecovered && !PlayerCombatBehaviour.BlockedHit)
                {
                    PlayerHitKind.Text = PlayerCombatBehaviour.CurrentHit.ToString();
                    PlayerHitKind.DrawText = true;
                    PlayerHitKind.SlideUpwards();
                    CreateHitEffectOnEnemy();
                }

                Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (Timer > NextHitDelay)
                {
                    PlayerHitKind.DrawText = false;
                    PlayerHitKind.ResetPosition();
                    PlayerCombatBehaviour.HasAttacked = false;
                    PlayerCombatBehaviour.HasRecovered = false;
                    CreatedAttackEffect = false;
                    Timer = 0;

                    if (enemyCombatBehaviour.Health > 0)
                    {
                        enemyCombatBehaviour.Attack(Player);
                        PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// depending on the hit kind of the player an other slash effect is shown on the enemy
        /// the enemy plays his hurt animation
        /// </summary>
        private void CreateHitEffectOnEnemy()
        {
            if (!CreatedAttackEffect)
            {
                if (PlayerCombatBehaviour.CurrentHit == HitKind.Ripper)
                    AttackEffect = new UIImage("Special", true);

                else if (PlayerCombatBehaviour.CurrentHit == HitKind.Scratch)
                    AttackEffect = new UIImage("Critical", true);

                else if (PlayerCombatBehaviour.CurrentHit == HitKind.Poke)
                    AttackEffect = new UIImage("Normal", true);

                AttackEffect.GetComponent<Transform>().Position += enemyImage.GetComponent<Transform>().Position;
                CreatedAttackEffect = true;
                enemyImage.SetAnimation(enemyImage.States[1]);
            }
        }

        /// <summary>
        /// same as the player attack algorithm
        /// the difference is that the slash effect is always created 
        /// and the hit kind text is always shown over the player head
        /// 
        /// after a short delay everything is resetted
        /// </summary>
        private void EnemyAttackAlgorithm(GameTime gameTime)
        {
            if (enemyCombatBehaviour.HasAttacked)
            {
                enemyHitKind.Text = enemyCombatBehaviour.CurrentHit.ToString();
                enemyHitKind.DrawText = true;
                enemyHitKind.SlideUpwards();

                CreateHitEffectOnPlayer();

                Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (Timer > NextHitDelay)
                {
                    enemyHitKind.DrawText = false;
                    enemyHitKind.ResetPosition();
                    enemyCombatBehaviour.HasAttacked = false;
                    PlayerCombatBehaviour.BlockedHit = false;
                    CreatedAttackEffect = false;
                    Timer = 0;
                }
            }
        }

        /// <summary>
        /// the enemy can create only one kind of slash effect (unlike the player who has three kinds)
        /// players hurt animation is only shown if the player doesn't defend himself 
        /// </summary>
        private void CreateHitEffectOnPlayer()
        {
            if (!CreatedAttackEffect)
            {
                AttackEffect = new UIImage("Enemy", true);
                AttackEffect.GetComponent<Transform>().Position += PlayerImage.GetComponent<Transform>().Position;
                CreatedAttackEffect = true;
                if (!PlayerCombatBehaviour.BlockedHit)
                    PlayerImage.SetAnimation(enemyImage.States[1]);
            }
        }

        private void OnKeyPressed(Keys key)
        {
            Vector2 arrowPosition = Arrow.GetComponent<Transform>().Position;

            switch (key)
            {
                case Keys.Up:
                case Keys.W:
                    SetArrowPosition(ref arrowPosition, 2, 1, 0);
                    Arrow.GetComponent<Transform>().Position = arrowPosition;
                    break;

                case Keys.Down:
                case Keys.S:
                    SetArrowPosition(ref arrowPosition, 0, 1, 2);
                    Arrow.GetComponent<Transform>().Position = arrowPosition;
                    break;

                case Keys.Enter:
                case Keys.Space:
                    if (!PlayerCombatBehaviour.HasAttacked && !enemyCombatBehaviour.HasAttacked)
                        CallAction(arrowPosition);
                    break;

                    // those are our cheats
                case Keys.F9:
                    PlayerCombatBehaviour.Health = int.MaxValue;
                    enemyCombatBehaviour.Health = int.MinValue;
                    break;
                case Keys.F8:
                    PlayerCombatBehaviour.HealValue = int.MaxValue;
                    break;
                case Keys.NumPad0:
                    enemyCombatBehaviour.Health += 1000;
                    break;
            }
        }

        private void SetArrowPosition(ref Vector2 arrowPos, int index1, int index2, int index3)
        {
            if (arrowPos == Arrow.Positions[index1])
                arrowPos = Arrow.Positions[index2];
            else if (arrowPos == Arrow.Positions[index2])
                arrowPos = Arrow.Positions[index3];
        }

        /// <summary>
        /// calls a specific action depending on which ability the player chooses
        /// </summary>
        private void CallAction(Vector2 arrowPos)
        {
            if (arrowPos == Arrow.Positions[0])
            {
                PlayerCombatBehaviour.Attack(enemy);
                enemyHP.Text = enemyCombatBehaviour.Health.ToString();
            }
            else if (arrowPos == Arrow.Positions[1])
            {
                PlayerCombatBehaviour.Block();
            }
            else
            {
                PlayerCombatBehaviour.Heal();
                PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
            }
        }

        private void DestroyObjects()
        {
            BattleMap.Destroy();

            Arrow.Destroy();
            PlayerHitKind.Destroy();
            enemyHitKind.Destroy();
            PlayerHP.Destroy();
            enemyHP.Destroy();

            PlayerImage.Destroy();
            enemyImage.Destroy();
        }
    }
}
