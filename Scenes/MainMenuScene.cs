﻿namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: The player can either exit or start in this scene
    /// </summary>
    class MainMenuScene : Scene
    {
        public override void LoadContent()
        {
            GameManager.AddObject(new MainMenuMap());
            GameManager.AddObject(new ExitButton());
            GameManager.AddObject(new StartButton());
            GameManager.AddObject(new Selector());
            SceneManager.ChangedScene = true;
        }
    }
}
