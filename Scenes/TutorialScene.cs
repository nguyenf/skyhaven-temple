﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: PowerPoint like Scene
    /// </summary>
    class TutorialScene : Scene
    {
        public override void LoadContent()
        {
            GameManager.AddObject(new TutorialMap());
            GameManager.AddObject(new Selector());
        }
    }
}
