﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this is our boss battle scene
    /// works the same way as the battle scene
    /// </summary>
    class BossScene : BattleScene
    {
        private Boss boss;
        private BossCombatBehaviour bossCombatBehaviour;
        private UIImage bossImage;
        private UIText bossHitKind;
        private UIText bossHP;

        public override void LoadContent()
        {
            SceneManager.ChangedScene = true;

            CreateObjects();
            SetStartPositions();
            bossCombatBehaviour.MaxCounter = 5;

            EventManager.OnUpdate += OnUpdate;
            InputManager.OnKeyPressed += OnKeyPressed;
        }

        private void CreateObjects()
        {
            BattleMap = new BattleMap();
            Arrow = Helper.Deserialize<UISelector>("Content/XML/Arrow");

            Player = (Player)GameManager.GetGameObject<Player>();
            PlayerCombatBehaviour = Player.GetComponent<PlayerCombatBehaviour>();
            PlayerImage = new UIImage("PlayerB", false);
            PlayerHitKind = Helper.Deserialize<UIText>("Content/XML/PlayerHitKindB");
            PlayerHP = Helper.Deserialize<UIText>("Content/XML/PlayerHP");

            boss = new Boss(50);
            bossCombatBehaviour = boss.GetComponent<BossCombatBehaviour>();
            bossImage = new UIImage();
            bossHitKind = Helper.Deserialize<UIText>("Content/XML/BossHitKind");
            bossHP = Helper.Deserialize<UIText>("Content/XML/BossHP");
        }

        private void SetStartPositions()
        {
            Arrow.GetComponent<Transform>().Position = Arrow.Positions[0];

            PlayerImage.GetComponent<Transform>().Position = new Vector2(100, 380);
            PlayerHitKind.SetPosition();
            PlayerHP.SetPosition();

            bossImage.GetComponent<Transform>().Position = new Vector2(480, 220);
            bossHitKind.SetPosition();
            bossHP.SetPosition();
        }

        private void OnUpdate(GameTime gameTime)
        {
            if (bossCombatBehaviour.IsCriticalLaunching)
                new ExclamationMark();

            PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
            bossHP.Text = bossCombatBehaviour.Health.ToString();

            if (bossCombatBehaviour.Health <= 0)
            {
                InputManager.OnKeyPressed -= OnKeyPressed;
                BattleEndCounter += gameTime.ElapsedGameTime.Milliseconds;

                if (BattleEndCounter >= BattleEndDelay)
                {
                    PlayerCombatBehaviour.HasAttacked = false;
                    DestroyObjects();
                    SceneManager.LoadScene<WinScene>(true);

                    EventManager.OnUpdate -= OnUpdate;
                }
            }

            else if (Player.GetComponent<PlayerCombatBehaviour>() == null)
            {
                InputManager.OnKeyPressed -= OnKeyPressed;
                EventManager.OnUpdate -= OnUpdate;
            }

            PlayerAttackAlgorithm(gameTime);
            BossAttackAlgorithm(gameTime);
        }

        private void PlayerAttackAlgorithm(GameTime gameTime)
        {
            if (PlayerCombatBehaviour.HasAttacked)
            {
                if (!PlayerCombatBehaviour.HasRecovered && !PlayerCombatBehaviour.BlockedHit)
                {
                    PlayerHitKind.Text = PlayerCombatBehaviour.CurrentHit.ToString();
                    PlayerHitKind.DrawText = true;
                    PlayerHitKind.SlideUpwards();
                    CreateHitEffectOnEnemy();
                }

                Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (Timer > NextHitDelay)
                {
                    PlayerHitKind.DrawText = false;
                    PlayerHitKind.ResetPosition();
                    PlayerCombatBehaviour.HasAttacked = false;
                    PlayerCombatBehaviour.HasRecovered = false;
                    CreatedAttackEffect = false;
                    Timer = 0;

                    if (bossCombatBehaviour.Health > 0)
                    {
                        bossCombatBehaviour.Attack(Player);
                        PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
                    }
                }
            }
        }

        private void CreateHitEffectOnEnemy()
        {
            if (!CreatedAttackEffect)
            {
                if (PlayerCombatBehaviour.CurrentHit == HitKind.Useless)
                    AttackEffect = new UIImage("Special", true);

                else if (PlayerCombatBehaviour.CurrentHit == HitKind.Weak)
                    AttackEffect = new UIImage("Critical", true);

                else if (PlayerCombatBehaviour.CurrentHit == HitKind.Pathetic)
                    AttackEffect = new UIImage("Normal", true);

                AttackEffect.GetComponent<Transform>().Position += bossImage.GetComponent<Transform>().Position;
                CreatedAttackEffect = true;
            }
        }

        private void BossAttackAlgorithm(GameTime gameTime)
        {
            if (bossCombatBehaviour.HasAttacked)
            {
                bossHitKind.Text = bossCombatBehaviour.CurrentHit.ToString();
                bossHitKind.DrawText = true;
                bossHitKind.SlideUpwards();

                CreateHitEffectOnPlayer();

                Timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (Timer > NextHitDelay)
                {
                    bossHitKind.DrawText = false;
                    bossHitKind.ResetPosition();
                    bossCombatBehaviour.HasAttacked = false;
                    PlayerCombatBehaviour.BlockedHit = false;
                    CreatedAttackEffect = false;
                    Timer = 0;
                }
            }
        }

        private void CreateHitEffectOnPlayer()
        {
            if (!CreatedAttackEffect)
            {
                AttackEffect = new UIImage("Critical", true, true);
                AttackEffect.GetComponent<Transform>().Position += PlayerImage.GetComponent<Transform>().Position;
                CreatedAttackEffect = true;
                PlayerImage.SetAnimation(bossImage.States[1]);
            }
        }

        private void OnKeyPressed(Keys key)
        {
            Vector2 arrowPosition = Arrow.GetComponent<Transform>().Position;

            switch (key)
            {
                case Keys.Up:
                case Keys.W:
                    SetArrowPosition(ref arrowPosition, 2, 1, 0);
                    Arrow.GetComponent<Transform>().Position = arrowPosition;
                    break;

                case Keys.Down:
                case Keys.S:
                    SetArrowPosition(ref arrowPosition, 0, 1, 2);
                    Arrow.GetComponent<Transform>().Position = arrowPosition;
                    break;

                case Keys.Enter:
                case Keys.Space:
                    if (!PlayerCombatBehaviour.HasAttacked && !bossCombatBehaviour.HasAttacked)
                        CallAction(arrowPosition);
                    break;
            }
        }

        private void SetArrowPosition(ref Vector2 arrowPos, int index1, int index2, int index3)
        {
            if (arrowPos == Arrow.Positions[index1])
                arrowPos = Arrow.Positions[index2];
            else if (arrowPos == Arrow.Positions[index2])
                arrowPos = Arrow.Positions[index3];
        }

        private void CallAction(Vector2 arrowPos)
        {
            if (arrowPos == Arrow.Positions[0])
            {
                PlayerCombatBehaviour.Attack(boss);
                bossHP.Text = bossCombatBehaviour.Health.ToString();
            }
            else if (arrowPos == Arrow.Positions[1])
            {
                PlayerCombatBehaviour.BlockedHit = true;
                SoundManager.Defend.Play();
            }
            else
            {
                PlayerCombatBehaviour.Health += PlayerCombatBehaviour.HealValue;
                PlayerHP.Text = PlayerCombatBehaviour.Health.ToString();
                PlayerCombatBehaviour.HasRecovered = true;
                SoundManager.Heal.Play();
            }

            PlayerCombatBehaviour.HasAttacked = true;
        }

        private void DestroyObjects()
        {
            BattleMap.Destroy();

            Arrow.Destroy();
            PlayerHitKind.Destroy();
            bossHitKind.Destroy();
            PlayerHP.Destroy();
            bossHP.Destroy();

            PlayerImage.Destroy();
            bossImage.Destroy();
        }
    }
}
