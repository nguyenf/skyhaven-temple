﻿using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    abstract class Scene
    {
        public virtual void LoadContent()
        {
        }
    }
}
