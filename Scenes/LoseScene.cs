﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Fast PowerPoint like Scene
    /// </summary>
    class LoseScene : Scene
    {
        public override void LoadContent()
        {
            GameManager.AddObject(new LoseMap());
            GameManager.AddObject(new Selector());
        }
    }
}
