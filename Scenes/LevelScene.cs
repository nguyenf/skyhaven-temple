﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex, Filip: We were working on the level a lot
    /// </summary>
    class LevelScene : Scene
    {
        /// <summary>
        /// These lists are important to not "hard code" our designed map
        /// </summary>
        public List<Gem> Gems = new List<Gem>();
        public List<Flame> Flames = new List<Flame>();
        public List<Enemy> Enemies = new List<Enemy>();
        public List<GameObject> GameObjects = new List<GameObject>();

        /// <summary>
        /// Filip: Deserialized the gameOBjects
        /// </summary>
        public override void LoadContent()
        {
            GameManager.AddObject(new Background());
            GameManager.AddObject(new NonCollidingObjects());
            Helper.Deserialize(ref Gems, "Content/XML/Gems");
            Helper.Deserialize(ref Flames, "Content/XML/Flames");
            Helper.Deserialize(ref Enemies, "Content/XML/Enemies");
            GameObjects.Add(Helper.Deserialize<Player>("Content/XML/PlayerInstance"));

            GameObjects.AddRange(Gems);
            GameObjects.AddRange(Flames);
            GameObjects.AddRange(Enemies);
            GameObjects.ForEach(g => g.SetStartPosition());

            GameManager.GameObjects.AddRange(GameObjects);
            GameObjects.Clear();
            GameManager.AddObject(new Camera(GameManager.GameObjects.Find(g => g is Player)));
        }
    }
}
