﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: PowerPoint like Scene
    /// </summary>
    class WinScene : Scene
    {
        public override void LoadContent()
        {
            SceneManager.ChangedScene = true;
            GameManager.AddObject(new WinMap());
            GameManager.AddObject(new Selector());
        }
    }
}
