﻿using System;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex, Filip: We were creating methods in this class which has been used several times
    /// </summary>
    public static class Helper
    {
        public static void LoadGameObject(int amount, GameObject gameObject)
        {
            for (int i = 0; i < amount; i++)
                GameManager.AddObject(gameObject);
        }

        /// <summary>
        /// Filip: This one has been used to be really precise when it comes to the position check
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        public static Vector2 Clamp(Vector2 vector, float distance)
        {
            if (vector.LengthSquared() > distance)
                return Vector2.Normalize(vector) * (float)Math.Sqrt(distance);
            else
                return vector;
        }

        /// <summary>
        /// This one is for fast debugging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlName"></param>
        public static void Serialize<T>(string xmlName) where T : new()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream fileStream = new FileStream(xmlName + ".xml", FileMode.Create);
            T instance = new T();
            serializer.Serialize(fileStream, instance);
            fileStream.Close();
        }

        /// <summary>
        /// This one has been used a lot of times in order to serialize
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <param name="xmlName"></param>
        public static void Serialize<T>(T instance, string xmlName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream fileStream = new FileStream(xmlName + ".xml", FileMode.Create);
            serializer.Serialize(fileStream, instance);
            fileStream.Close();
        }

        /// <summary>
        /// Alex
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlName"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string xmlName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream fileStream = new FileStream(xmlName + ".xml", FileMode.Open);
            T instance = (T)serializer.Deserialize(fileStream);
            fileStream.Close();
            return instance;
        }

        /// <summary>
        /// Filip
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <param name="xmlName"></param>
        public static void Deserialize<T>(ref T instance, string xmlName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream fileStream = new FileStream(xmlName + ".xml", FileMode.Open);
            instance = (T)serializer.Deserialize(fileStream);
            fileStream.Close();
        }
    }
}
