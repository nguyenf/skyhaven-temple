﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Works with the animation component
    /// </summary>
    class SpriteFrame
    {
        public string Name;
        public Rectangle Bounds = new Rectangle();
    }
}
