﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: Basic tile for out tilemap(s)
    /// </summary>
    class Tile : GameObject
    {
        // those are positions of a tile on the sprite sheet
        public float TileSheetX;
        public float TileSheetY;

        private Transform transform;
        private BoxCollider collider;

        public Tile(float x, float y, float positionX, float positionY, bool hasCollider = false)
        {
            TileSheetX = x;
            TileSheetY = y;

            transform = AddComponent<Transform>();
            transform.Position = new Vector2(positionX * TileMapJapaneseVillage.TileWidth, positionY* TileMapJapaneseVillage.TileHeight);

            if (hasCollider)
            {
                collider = AddComponent<BoxCollider>();
                collider.Width = TileMapJapaneseVillage.TileWidth;
                collider.Height = TileMapJapaneseVillage.TileHeight;
            }
        }
    }
}
