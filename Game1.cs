﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Skyhaven_v2
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
           
           graphics.PreferredBackBufferHeight = 600;
           graphics.PreferredBackBufferWidth = 800;
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            GameManager.Game = this;
            GameManager.GraphicsDevice = GraphicsDevice;
            GameManager.Content = Content;
            SoundManager.LoadContent();
            SpriteFontManager.LoadContent();
            SceneManager.LoadScene<MainMenuScene>();
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            GameManager.ApplyChanges();
            EventManager.Update(gameTime);
            InputManager.Update();
            SoundManager.PlayBackgroundMusic();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            EventManager.Draw(spriteBatch);
            base.Draw(gameTime);
        }
    }
}
