﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class is used in the battle scenes
    /// </summary>
    public class UISelector : GameObject
    {
        public Vector2[] Positions = new Vector2[3];

        private Transform transform;
        private Renderer renderer;

        public UISelector()
        {
            Name = "Arrow";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
        }
    }
}
