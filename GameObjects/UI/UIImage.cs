﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: this class draws images in our battle scenes
    /// </summary>
    class UIImage : GameObject
    {
        public Color Color;
        public string[] States = { "_I", "_H" };

        private Transform transform;
        private Animation animation;
        private bool isAttackEffect;
        private string currentState;

        /// <summary>
        /// draws either the player or the enemy if the bools are false
        /// else it draws either the slash animation of the player/enemy or the boss
        /// </summary>
        public UIImage(string name, bool isAttackEffect, bool isBossEffect = false)
        {
            Name = name;

            transform = AddComponent<Transform>();
            animation = AddComponent<Animation>();
            this.isAttackEffect = isAttackEffect;

            if (!isAttackEffect)
            {
                animation.LoadContent(GameManager.LoadTexture("BattleSceneSpriteAtlas"), "/XML/BattleSceneSpriteAtlas.xml");
                animation.FrameDelay = 50;
                currentState = States[0];
                animation.PlayAnimation(currentState);
            }

            else if (!isBossEffect)
            {
                animation.LoadContent(GameManager.LoadTexture("Attacks"), "/XML/Attacks.xml");
                animation.FrameDelay = 100;
                animation.PlayAnimation("_");
                transform.Position = new Vector2(-120, -100);
            }
            else
            {
                animation.LoadContent(GameManager.LoadTexture("BossAttacks"), "/XML/Attacks.xml");
                animation.FrameDelay = 100;
                animation.PlayAnimation("_");
                transform.Position = new Vector2(-120, -100);
            }

            EventManager.OnUpdate += OnUpdate;
        }

        /// <summary>
        /// draws the boss, if the other constructor isn't used
        /// </summary>
        public UIImage()
        {
            Name = "Boss";

            transform = AddComponent<Transform>();
            animation = AddComponent<Animation>();

            animation.LoadContent(GameManager.LoadTexture("BossSheet"), "/XML/BossSheet.xml");
            animation.FrameDelay = 50;
            currentState = "_";
            animation.PlayAnimation(currentState);
        }

        /// <summary>
        /// changes the animation state
        /// </summary>
        public void SetAnimation(string state)
        {
            currentState = state;
            animation.PlayAnimation(state);
        }

        /// <summary>
        /// when the animation ends:
        /// if the image is a slash effect, it destroys itself
        /// if the current Animation is the hurt animation, it sets back to idle animation
        /// </summary>
        public void OnUpdate(GameTime gameTime)
        {
            if (animation.IsOver && isAttackEffect)
                Destroy();
            else if (animation.IsOver && currentState == States[1])
                SetAnimation(States[0]);
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= OnUpdate;
            base.Destroy();
        }
    }
}
