﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Now the player should know when he or she should use defend!
    /// </summary>
    class ExclamationMark : GameObject
    {
        private Renderer renderer;
        private Transform transform;
        private int timer;

        public bool StartRender { get; set; }

        public ExclamationMark()
        {
            Name = "ExclamationMark";
            transform = AddComponent<Transform>();
            transform.Position = new Vector2(450, 50);
            renderer = AddComponent<Renderer>();
            EventManager.OnUpdate += Update;
        }

        /// <summary>
        /// Destroys itself after 700 milliseconds
        /// </summary>
        /// <param name="gameTime"></param>
        private void Update(GameTime gameTime)
        {
            if (!(SceneManager.Scene is BattleScene))
                Destroy();

            timer += gameTime.ElapsedGameTime.Milliseconds;

            if (timer >= 700)
            {
                timer = 0;
                Destroy();
            }
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
