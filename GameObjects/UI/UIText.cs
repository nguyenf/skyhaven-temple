﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class draws text
    /// </summary>
    public class UIText : GameObject
    {
        public Vector2 StartPos;
        public Color Color;
        public string Text;
        public bool DrawText;

        private Transform transform;

        public UIText()
        {
            transform = AddComponent<Transform>();
            EventManager.OnDraw += OnDraw;
        }

        public UIText(string name)
        {
            Name = name;

            transform = AddComponent<Transform>();
            EventManager.OnDraw += OnDraw;
        }

        public void SetPosition()
        {
            transform.Position = StartPos;
        }

        public void SlideUpwards()
        {
            transform.Translate(0, -1);
        }

        public void ResetPosition()
        {
            transform.Position = StartPos;
        }

        private void OnDraw(SpriteBatch spriteBatch)
        {
            if (DrawText)
                spriteBatch.DrawString(SpriteFontManager.ActionJackson, Text, transform.Position, Color);
        }

        public override void Destroy()
        {
            EventManager.OnDraw -= OnDraw;
            base.Destroy();
        }
    }
}
