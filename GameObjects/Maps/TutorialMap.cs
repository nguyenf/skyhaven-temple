﻿using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Fast and visible TutorialScreen
    /// </summary>
    class TutorialMap : GameObject
    {
        private Renderer renderer;
        private Transform transform;

        public TutorialMap()
        {
            Name = "TutorialMap";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            InputManager.OnKeyPressed += ProcessInput;
        }

        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                default:
                    SceneManager.LoadScene<LevelScene>(true);
                    break;
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
