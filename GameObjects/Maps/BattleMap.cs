﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this is the background of our battle scene
    /// </summary>
    class BattleMap : GameObject
    {
        private Transform transform;
        private Renderer renderer;

        public BattleMap()
        {
            Name = "BattleMap";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
        }
    }
}
