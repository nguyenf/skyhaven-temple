﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: LoseScreen
    /// </summary>
    class LoseMap : GameObject
    {
        private Renderer renderer;
        private Transform transform;

        public LoseMap()
        {
            Name = "LoseMap";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            InputManager.OnKeyPressed += ProcessInput;
        }

        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                default:
                    SceneManager.LoadScene<CreditsScene>();
                    break;
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
