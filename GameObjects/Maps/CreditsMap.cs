﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Credits 
    /// </summary>
    class CreditsMap : GameObject
    {
        private Renderer renderer;
        private Transform transform;

        public CreditsMap()
        {
            Name = "CreditsMap";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            InputManager.OnKeyPressed += ProcessInput;
        }

        private void ProcessInput(Keys key)
        {
            switch (key)
            {
                default:
                    GameManager.Exit();
                    break;
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= ProcessInput;
            base.Destroy();
        }
    }
}
