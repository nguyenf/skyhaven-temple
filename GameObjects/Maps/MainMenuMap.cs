﻿namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: MainMenuScreen
    /// </summary>
    class MainMenuMap : GameObject
    {
        private Renderer renderer;
        private Transform transform;

        public MainMenuMap()
        {
            Name = "MainMenuMap";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
        }
    }
}
