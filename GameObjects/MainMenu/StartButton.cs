﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    class StartButton : GameObject
    {
        private Renderer renderer;
        private Transform transform;
        private BoxCollider boxCollider;

        public StartButton()
        {
            Name = "StartButton";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            transform.Position = new Vector2(100, 230);
            boxCollider = AddComponent<BoxCollider>();
        }
    }
}
