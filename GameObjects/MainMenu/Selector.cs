﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: It looks cooler to have his own mouse in your own game
    /// </summary>
    class Selector : GameObject
    {
        private float timer;
        private Renderer renderer;
        private Transform transform;
        private BoxCollider boxCollider;
        private GameObject currentGameObject;
        private SelectorKeyInput selectorKeyInput;

        public Selector()
        {
            Name = "Selector";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            selectorKeyInput = AddComponent<SelectorKeyInput>();
            boxCollider = AddComponent<BoxCollider>();
            InputManager.OnKeyPressed += PressKey;
            boxCollider.OnCollisionEnter += EnterCollision;
            boxCollider.OnCollisionExit += ExitCollision;
            EventManager.OnUpdate += Update;

        }

        private void Update(GameTime gameTime)
        {
            ProcessMouseClick(gameTime);
        }

        private void ExitCollision(GameObject gameObject)
        {
            currentGameObject = null;
        }

        private void EnterCollision(GameObject gameObject)
        {
            currentGameObject = gameObject;
        }

        /// <summary>
        /// For all scenes where the selector is, there is a scene to load
        /// </summary>
        /// <param name="gameTime"></param>
        private void ProcessMouseClick(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();

            LoadSceneByClick<TutorialScene, LevelScene>(mouseState, gameTime);
            LoadSceneByClick<WinScene, CreditsScene>(mouseState, gameTime);
            LoadSceneByClick<LoseScene, CreditsScene>(mouseState, gameTime);
            LoadSceneByClick<CreditsScene, MainMenuScene>(mouseState, gameTime);

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (currentGameObject is ExitButton)
                    GameManager.Exit();
                if (currentGameObject is StartButton)
                    SceneManager.LoadScene<TutorialScene>(true);
            }
        }

        private void LoadSceneByClick<T, K>(MouseState mouseState, GameTime gameTime) where K : Scene, new()
        {
            if (SceneManager.Scene is T)
            {
                timer += gameTime.ElapsedGameTime.Milliseconds;

                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    if (timer >= 500)
                    {
                        if (SceneManager.Scene is CreditsScene)
                            GameManager.Exit();

                        SceneManager.LoadScene<K>(true);
                        timer = 0;
                    }
                }
            }
        }

        private void PressKey(Keys key)
        {
            switch (key)
            {
                case Keys.Space:
                case Keys.Enter:
                    if (currentGameObject is ExitButton)
                        GameManager.Exit();
                    if (currentGameObject is StartButton)
                        SceneManager.LoadScene<TutorialScene>(true);
                    break;
            }
        }

        public override void Destroy()
        {
            InputManager.OnKeyPressed -= PressKey;
            boxCollider.OnCollisionEnter -= EnterCollision;
            boxCollider.OnCollisionExit -= ExitCollision;
            EventManager.OnUpdate -= Update;
            base.Destroy();
        }
    }
}
