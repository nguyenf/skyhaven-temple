﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    class ExitButton : GameObject
    {
        private Renderer renderer;
        private Transform transform;
        private BoxCollider boxCollider;

        public ExitButton()
        {
            Name = "ExitButton";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            transform.Position = new Vector2(600, 230);
            boxCollider = AddComponent<BoxCollider>();
        }
    }
}
