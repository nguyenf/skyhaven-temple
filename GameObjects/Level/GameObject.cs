﻿using System;
using System.Reflection;
using System.Collections;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: After switching the architecture, the gameObject class has been modified quiet some time
    /// in order to serialize some gameObjects
    /// </summary>
    public class GameObject
    {
        protected Hashtable components = new Hashtable();

        public Vector2 StartPosition;
        public Vector2 TargetPosition;

        public string Name { get; set; }

        public bool HasComponent<T>()
        {
            return components.ContainsKey(typeof(T));
        }

        /// <summary>
        /// This was important in order to set the postion after deserializing the gameObjects of the Level out of 
        /// the Enemy.xml, Player.xml, Flames.xml and Gems.xml
        /// </summary>
        public void SetStartPosition()
        {
            if (HasComponent<FlameMovement>())
                GetComponent<FlameMovement>().SetTargetPositions(StartPosition.X, StartPosition.Y, TargetPosition.X, TargetPosition.Y);

            if (HasComponent<Transform>())
                GetComponent<Transform>().Position = StartPosition;
        }

        public T AddComponent<T>() where T : Component, new()
        {
            if (!HasComponent<T>())
            {
                T component = new T();
                component.SetGameObject(this);
                components.Add(typeof(T), component);
                Invoke(component, typeof(T));
            }

            return (T)components[typeof(T)];
        }

        public void RemoveComponent<T>()
        {
            components.Remove(typeof(T));
        }

        public T GetComponent<T>()
        {
            return (T)components[typeof(T)];
        }

        public virtual void Destroy()
        {
            GameManager.Remove(this);
            Component[] temp = new Component[components.Count];
            components.Values.CopyTo(temp, 0);

            foreach (var component in temp)
                component.Destroy();
        }

        private void Invoke(object component, Type type)
        {
            MethodInfo method = type.GetMethod("Start", BindingFlags.NonPublic | BindingFlags.Instance);
            if (method != null)
                method.Invoke(component, null);
        }
    }
}
