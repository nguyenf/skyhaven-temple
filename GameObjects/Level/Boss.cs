﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex, Filip: this is our boss class
    /// </summary>
    class Boss : GameObject
    {
        private Transform transform;
        private Animation animation;
        private BoxCollider boxCollider;
        private BossCombatBehaviour bossCombatBehaviour;

        /// <summary>
        /// this constructor is used in our level scene
        /// when the boss spawns, other objects like gems and flames will be destroyed
        /// </summary>
        public Boss(float x, float y)
        {
            Name = "Boss";
            DestroyOtherObjects();
            transform = AddComponent<Transform>();
            transform.Position = new Vector2(x, y);
            animation = AddComponent<Animation>();
            animation.LoadContent(GameManager.LoadTexture("BossAtlas"), "/XML/BossAtlas.xml");
            animation.FrameDelay = 70;
            animation.PlayAnimation("_");
            boxCollider = AddComponent<BoxCollider>();
        }

        /// <summary>
        /// this constructor is used in our boss battle scene
        /// </summary>
        public Boss(int Power = int.MaxValue, int Health = 300)
        {
            Name = "Boss";
            bossCombatBehaviour = AddComponent<BossCombatBehaviour>();
            bossCombatBehaviour.SetPoints(Power, Health);
        }

        private void DestroyOtherObjects()
        {
            foreach (var gameObject in GameManager.GameObjects)
                if (gameObject is Flame || gameObject is Gem)
                    gameObject.Destroy();
        }

        public override void Destroy()
        {
            base.Destroy();
        }
    }
}
