﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class draws the tiles which has no collider in our level scene
    /// </summary>
    class NonCollidingObjects : GameObject
    {
        private TileMapJapaneseVillage tileMap;
        private Renderer renderer;

        public NonCollidingObjects()
        {
            tileMap = AddComponent<TileMapJapaneseVillage>();
            tileMap.SetMapField("CM_NonCollidingObjects", 50, 38);
            renderer = AddComponent<Renderer>();
        }
    }
}
