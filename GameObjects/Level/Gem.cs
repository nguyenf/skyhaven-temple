﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Base class of the gem
    /// </summary>
    public class Gem : GameObject
    {
        private Renderer renderer;
        private Transform transform;
        private BoxCollider boxCollider;
        private GemMovement gemMovement;

        public Gem()
        {
            Name = "Gem";
            transform = AddComponent<Transform>();
            renderer = AddComponent<Renderer>();
            boxCollider = AddComponent<BoxCollider>();
            gemMovement = AddComponent<GemMovement>();
            boxCollider.OnCollisionEnter += OnCollisionEnter;
        }

        private void OnCollisionEnter(GameObject gameObject)
        {
            if (!(gameObject is Player))
                return;

            gameObject.GetComponent<PlayerCombatBehaviour>().Health += 15;
            SoundManager.Collect.Play();
            Destroy();
        }

        public override void Destroy()
        {
            boxCollider.OnCollisionEnter -= OnCollisionEnter;
            base.Destroy();
        }
    }
}
