﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Base class of the Overkill
    /// </summary>
    class Overkill : GameObject
    {
        private const int StartVelocityRange = 3;
        private const int AttackPower = 4;

        private Transform transform;
        private OverkillMovement overkillMovement;
        private Flocking flocking;
        private BoxCollider boxCollider;
        private Combat combat;

        public Overkill(Vector2 position)
        {
            Name = "Overkill";
            transform = AddComponent<Transform>();
            transform.Position = position;
            InitRandomStartVelocity();
            overkillMovement = AddComponent<OverkillMovement>();
            flocking = AddComponent<Flocking>();
            boxCollider = AddComponent<BoxCollider>();
            combat = AddComponent<Combat>();
            combat.SetPoints(AttackPower, 0);
            boxCollider.OnCollisionEnter += EnterCollision;
            EventManager.OnUpdate += Update;
        }

        private void Update(GameTime gameTime)
        {
            SearchForEnemies();
        }

        /// <summary>
        /// Attacks the enemy and destroys itself afterwards.
        /// </summary>
        /// <param name="gameObject"></param>
        private void EnterCollision(GameObject gameObject)
        {
            if (!(gameObject is Enemy))
                return;

            if (GameManager.GameObjects.Contains(gameObject))
            {
                combat.Attack(gameObject);
                Destroy();
            }
        }

        /// <summary>
        /// In order to make the flocking more natural, a random velocity is going to be 
        /// created once the Overkill is added to the list
        /// </summary>
        private void InitRandomStartVelocity()
        {
            int range = GameManager.Random.Next(-StartVelocityRange, StartVelocityRange);

            transform.Velocity = new Vector2(range, range);
        }

        /// <summary>
        /// I simply did not want the Overkills to stay when there are no enemyies
        /// </summary>
        private void SearchForEnemies()
        {
            if (!GameManager.GameObjects.Exists(g => g is Enemy))
                Destroy();
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            boxCollider.OnCollisionEnter -= EnterCollision;
            base.Destroy();
        }
    }
}
