﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: We both spended time as a team in order to make it work
    /// </summary>
    public class Camera : GameObject
    {
        private Transform transform;
        private Follow follow;

        public Camera()
        {
            transform = AddComponent<Transform>();
            follow = AddComponent<Follow>();
        }
        public Camera(GameObject obj)
        {
            transform = AddComponent<Transform>();
            follow = AddComponent<Follow>();
            CenterCameraOnObject(obj);
        }

        public Matrix GetMatrix()
        {
            return Matrix.CreateTranslation(new Vector3(-transform.Position, 0));
        }

        private void CenterCameraOnObject(GameObject obj)
        {
            transform.Position = obj.GetComponent<Transform>().Position - new Vector2(GameManager.GraphicsDevice.Viewport.Width / 2, GameManager.GraphicsDevice.Viewport.Height / 2);
        }
    }
}
