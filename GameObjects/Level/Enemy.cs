﻿using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Base class of the enemy
    /// </summary>
    public class Enemy : GameObject
    {
        private Status status;
        private Transform transform;
        private BoxCollider boxCollider;
        private EnemyBehaviour enemyBehaviour;
        private EnemyCombatBehaviour enemyCombatBehaviour;

        public Enemy()
        {
            Name = "Enemy";
            transform = AddComponent<Transform>();
            enemyCombatBehaviour = AddComponent<EnemyCombatBehaviour>();
            enemyCombatBehaviour.MaxCounter = GameManager.Random.Next(2, 5);
            enemyCombatBehaviour.SetPoints(10, 100);
            enemyBehaviour = AddComponent<EnemyBehaviour>();
            boxCollider = AddComponent<BoxCollider>();
            status = AddComponent<Status>();
            EventManager.OnUpdate += Update;
            boxCollider.OnCollisionEnter += OnCollisionEnter;
        }

        private void Update(GameTime gameTime)
        {
            DelayDeath(gameTime);
        }

        private void OnCollisionEnter(GameObject gameObject)
        {
            if (gameObject is Player)
            {
                GameManager.ActiveEnemy = this;
                SceneManager.SavedScene = SceneManager.Scene;
                SceneManager.LoadScene<BattleScene>();
            }
        }

        private void DelayDeath(GameTime gameTime)
        {
            if (enemyBehaviour.IsDead)
                if (GetComponent<Animation>().IsOver)
                    Destroy();
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            boxCollider.OnCollisionEnter -= OnCollisionEnter;
            base.Destroy();
        }
    }
}
