﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Skyhaven_v2
{
    /// <summary>
    /// Alex: this class draws the floor and walls in our level scene 
    /// </summary>
    class Background : GameObject
    {
        private TileMapJapaneseVillage floor;
        private Renderer renderer;

        public Background()
        {
            floor = AddComponent<TileMapJapaneseVillage>();
            floor.SetMapField("CM_LevelBackground", 50, 38);
            renderer = AddComponent<Renderer>();
        }
    }
}
