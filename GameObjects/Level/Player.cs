﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip, Alex: Most important class 
    /// </summary>
    public class Player : GameObject
    {
        private const int Amount = 10;
        private const int Area = 100;
        private const int CoolDownOverkill = 5;

        private Status status;
        private GameObject other;
        private CoolDown coolDown;
        private Animation animation;
        private Transform transform;
        private BoxCollider boxCollider;
        private PlayerMovement playerMovement;
        private PlayerBehaviour playerKeyInput;
        private GameObjectLoader gameObjectLoader;
        private PlayerCombatBehaviour playerCombatBehaviour;

        public Player()
        {
            Name = "Player";
            transform = AddComponent<Transform>();
            playerCombatBehaviour = AddComponent<PlayerCombatBehaviour>();
            playerCombatBehaviour.SetPoints(20, 100);
            animation = AddComponent<Animation>();
            playerMovement = AddComponent<PlayerMovement>();
            playerKeyInput = AddComponent<PlayerBehaviour>();
            boxCollider = AddComponent<BoxCollider>();
            gameObjectLoader = AddComponent<GameObjectLoader>();
            coolDown = AddComponent<CoolDown>();
            coolDown.Duration = CoolDownOverkill;
            status = AddComponent<Status>();
            EventManager.OnUpdate += Update;
            boxCollider.OnCollisionEnter += OnCollisionEnter;
            boxCollider.OnCollisionExit += OnCollisionExit;
        }

        /// <summary>
        /// Summons the boss when there are no enemies alive
        /// </summary>
        private void Update(GameTime gameTime)
        {
            if (!GameManager.GameObjects.Exists(g => g is Boss))
                if (!IsEnemyAlive() && SceneManager.Scene is LevelScene)
                    GameManager.AddObject(new Boss(736f, 544f));
        }

        /// <summary>
        /// CAST OVERKILL!!! But the player loses HP when activated
        /// </summary>
        public void CastOverkill()
        {
            if (!coolDown.IsCoolingDown && IsEnemyAlive() && playerCombatBehaviour.Health > 10)
            {
                playerCombatBehaviour.Health -= Amount;
                gameObjectLoader.LoadOverkill(Amount, Area);
                coolDown.IsCoolingDown = true;
            }
        }

        private void OnCollisionExit(GameObject gameObject)
        {
            other = null;
            playerMovement.CanIMove = true;
        }
        private void OnCollisionEnter(GameObject gameObject)
        {
            if (other != gameObject)
                other = gameObject;

            CheckCollision();
        }

        /// <summary>
        /// All neccessary collisions for the player, making it look more natural
        /// </summary>
        private void CheckCollision()
        {
            // for each gem the player collects he's able to heal more HP during battle 
            if (other is Gem)
                playerCombatBehaviour.HealValue += 4;

            if (other is Boss)
                SceneManager.LoadScene<BossScene>(GameManager.GetGameObject<Player>());

            if (other is Flame)
            {
                playerCombatBehaviour.Health -= 10;
                SoundManager.Ouch.Play();
            }

            // made by Alex (actually the only thing I made in this class)
            // makes sure that the player can't move outside the map
            // when player collides with the wall he won't be able to move until he exits the collision
            if (other is Tile)
            {
                playerMovement.CanIMove = false;
                BoxCollider otherCollider = other.GetComponent<BoxCollider>();

                if (transform.Position.X < otherCollider.Width)
                    transform.Position = new Vector2(otherCollider.Width + 1, transform.Position.Y);
                if (transform.Position.X + boxCollider.Width >= GameManager.GraphicsDevice.Viewport.Width * 2 - otherCollider.Width)
                    transform.Position = new Vector2(GameManager.GraphicsDevice.Viewport.Width * 2 - otherCollider.Width -boxCollider.Width - 1, transform.Position.Y);

                if (transform.Position.Y < otherCollider.Height)
                    transform.Position = new Vector2(transform.Position.X, otherCollider.Height + 1);
                if (transform.Position.Y + boxCollider.Height >= GameManager.GraphicsDevice.Viewport.Height * 2 - otherCollider.Height)
                    transform.Position = new Vector2(transform.Position.X, GameManager.GraphicsDevice.Viewport.Height * 2 - otherCollider.Height - boxCollider.Height - 1);
            }
        }

        private bool IsEnemyAlive()
        {
            if (GameManager.GameObjects.Exists(g => g is Enemy))
                return true;
            return false;
        }

        public override void Destroy()
        {
            EventManager.OnUpdate -= Update;
            boxCollider.OnCollisionEnter -= OnCollisionEnter;
            boxCollider.OnCollisionExit -= OnCollisionExit;
            base.Destroy();
        }
    }
}
