﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Skyhaven_v2
{
    /// <summary>
    /// Filip: Base class of the flame
    /// </summary>
    public class Flame : GameObject
    {
        private Transform transform;
        private BoxCollider boxCollider;
        private FlameMovement flameMovement;

        public Flame()
        {
            Name = "Flame";
            transform = AddComponent<Transform>();
            flameMovement = AddComponent<FlameMovement>();
            boxCollider = AddComponent<BoxCollider>();
        }
    }
}
